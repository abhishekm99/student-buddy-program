-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: buddyproject
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `articles`
--

DROP TABLE IF EXISTS `articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `articles` (
  `id` int NOT NULL AUTO_INCREMENT,
  `articleName` varchar(255) NOT NULL,
  `imageUrl` varchar(255) NOT NULL,
  `tags` text NOT NULL,
  `shortDesc` text NOT NULL,
  `articleUrl` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `articles`
--

LOCK TABLES `articles` WRITE;
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
INSERT INTO `articles` VALUES (1,'Manipal for the Traveller','https://i1.wp.com/themitpost.com/wp-content/uploads/2017/05/10548159_977173148966593_3242330007571489992_o.jpg?resize=1024%2C683&ssl=1','Fresher\'s Content, Freshers, leisure, Manipal, MIT, places, to go, to see, Travel','The edgy daredevil of a teenager in you might inspire the inexplicable urge to dive headfirst into college. Do not be fooled. Instead, fuel your inner grandma and mentally stock up on a list of places to explore in your new, semi-permanent hometown, Manipal','https://themitpost.com/manipal-traveller/'),(2,'Wheres and Whats—A Shopping and Eating Guide to Manipal','https://i2.wp.com/themitpost.com/wp-content/uploads/2016/05/13275051_575172139327601_2113348008_o.jpg?resize=768%2C511&ssl=1','Campus, eateries, food, Fresher\'s Content, Manipal, MIT, restaurants, shopping, stores','Good restaurants for all budgets, the speciality of each eatery, details of departmental stores and the kind of variety each has to offer, places to shop, and bookstores that are within the campus—these are some of the things the incoming freshman needs. So, here is a handy guide. Happy eating and shopping!','https://themitpost.com/wheres-whats-shopping-eating-guide-manipal/'),(3,'The Manipal Handbook','https://i0.wp.com/themitpost.com/wp-content/uploads/2016/05/New-Doc-2017-05-25_3.jpg?resize=1024%2C963&ssl=1','essentials, fresher, Fresher\'s Content, guide, Manipal, met, Placements, suitcase, traveller','At The Post, we know that the anticipatory glee before coming to college can result in a state of panic laced excitement. We also know that MIT can offer you the best years of your life. So, to help rein in the panic that comes with leaving home, here’s everything you need to know about MIT.','https://themitpost.com/the-manipal-handbook/'),(4,'Hostel Life and How to Survive it','https://i0.wp.com/schoolbook.getmyuni.com/assets/images/rev_img/23871__3018/147695512898.jpg?resize=558%2C370','campus life in manipal, fresher, Fresher\'s Content, hostels, Manipal, MIT','Your home away from home, these buildings would probably relate the best stories if they could. Choosing the right hostel in the first year can go a long way in shaping your college life.','https://themitpost.com/hostel-life-survive/'),(5,'The Final Test—Placements in MIT','https://content.timesjobs.com/photo/66737138/trending/preparing-for-campus-placements-let-industry-experts-guide-you.jpg','Campus, Freshers, MIT, Placements','College placement is one of the most important factors to be considered when choosing a college. MIT students, at the end of their college term, have the chance to take up jobs in many major companies and can also expect to receive the help needed to be ready for it. Here is a comprehensive piece about where MIT stands in this matter.','https://themitpost.com/the-final-test/'),(6,'Your Home, in a Suitcase','https://studybreaks.com/wp-content/uploads/2016/07/o-COLLEGE-MOVE-facebook.jpg','campus life in manipal, clothes, fresher, Fresher\'s Content, luggage, Manipal, MIT, packing','Packing away for a new chapter of life is always difficult and unsettling. We try to help you out with a list of all that is needed, and where you can get them from.','https://themitpost.com/your-home-in-a-suitcase/'),(7,'Student Bodies in Manipal—Clubs, Projects, Sports, and More','http://www.wilkescc.edu/wp-content/uploads/2015/08/student-activities_960x350.jpg','activities, clubs, Cultural, fresher, organisations, societies, Sports, Students, technical','The MIT Post’s latest endeavor is to create a database of the myriad clubs, student societies, and student projects present in Manipal. We plan to increase the awareness of every MITian about the activities going on inside and outside the campus. Here is a list of all the different student bodies, both official, non official and hobby clubs, that an MITian can be a part of. We hope that you find a club which is tailored to your skills and interests.','https://themitpost.com/student-bodies-manipal/');
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-15 12:03:10
