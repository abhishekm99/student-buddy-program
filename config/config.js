module.exports = {
    region: 'us-east-1',
    accessKeyId: process.env.awsaccesskey,
    secretAccessKey: process.env.awssecretkey
}