var express = require('express');
var router = express.Router();
const isAuth = require('../middlewares/is-auth');

const loginController = require('../controllers/admin/login');
const buddyController = require('../controllers/buddy');

const dotenv = require('dotenv');


//API

//Announcements

// router.get('/announcements', eventsController.getAnnouncements);
// router.get('/colleges', eventsController.returnColleges);
router.get('/interests', isAuth, loginController.getInterests);
router.get('/branches', isAuth, buddyController.getBranchList);
router.get('/articles', isAuth, buddyController.getArticles);

router.get('/counts', isAuth, buddyController.getCounts);



module.exports = router;