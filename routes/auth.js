var express = require('express');
var router = express.Router();
const multer = require("multer")

var loginController = require('../controllers/admin/login');
var buddyController = require('../controllers/buddy');
// const eventController = require('../controllers/events');
const isAuth = require('../middlewares/is-auth');
const { check, body } = require('express-validator/check');

const fileFilter = (req,file,cb) => {
    if(file.mimetype.startsWith("image")){
        cb(null, true);
    } else {
        cb(null, false);
    }
}
var storage = multer.diskStorage({ 
    destination: (req, file, cb) => {
        cb(null, __dirname + '/../public/uploads/')
      },
    filename: function (req, file, cb) { 
      console.log(file)
      let filename = file.originalname.split('.')[0]
      filename = filename.split(' ')[0]
      cb(null, filename + "-" + Date.now()+".jpg") 
    } 
  }) 

var upload = multer({ storage: storage,fileFilter: fileFilter})
// var uploadedit = multer({ storage: storage })

router.get('/', loginController.getLogin);
router.get('/login', loginController.getLogin);
router.post('/login', loginController.postLogin);
router.get('/dashboard', isAuth, loginController.getDashboard);
// router.get('/register',  loginController.getRegister);
// router.post('/register',  loginController.postRegister);
router.get('/fpwd', loginController.getFPwd);
router.post('/fpwd', loginController.postReset);
router.get('/resetp/:token', loginController.getNewPassword);
router.post('/rpwd', loginController.postNewPassword);
router.get('/confirmemail', loginController.verifyEmail);
router.post('/register/project',isAuth, loginController.registerProject);
router.post('/register/project/withoutbranch',isAuth, loginController.registerProjectWithoutBranch);
router.get('/getpermission', isAuth, buddyController.getRequestData);
router.post('/requestpermission', isAuth, buddyController.raiseRequest);
router.post('/sendmessage', isAuth, buddyController.createMessage);
router.get('/messages/all', isAuth, buddyController.getMessages);
router.get('/approverequest', isAuth, buddyController.approveContactRequest);
router.get('/notifications', loginController.getNotification);
router.post('/notifications/markasread', buddyController.markNotification);
router.get('/council/getAll', loginController.getAllApplicants);
router.post('/resendverification', loginController.resendVerification);


// router.get('/council/buddyfix', isAuth, loginController.councilbuddyfix);

router.post('/logout', isAuth, loginController.postLogout);
module.exports = router;