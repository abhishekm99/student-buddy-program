var express = require('express');
const { check, validationResult } = require('express-validator');
var router = express.Router();
// const clubsController = require('../controllers/clubs');
// const eventsController = require('../controllers/events');
const User = require('../models/users');
const loginController = require('../controllers/admin/login')

const captcha = require('../helpers/recaptchaRedirect');

const dotenv = require('dotenv');

dotenv.config();

/* GET home page. */
router.get('/', function(req, res) {
  res.render('landing/landing', {title: 'Student Buddy Program', notMain: true})
});

router.get('/aboutmanipal', loginController.getAboutManipal);


// router.get('/clubs/technical', clubsController.getTechnicalClubs);

// router.get('/clubs/cultural', clubsController.getCulturalClubs);

// router.get('/clubs/sports', clubsController.getSportsClubs);

// router.get('/clubs/sp', clubsController.getOtherClubs);

// router.get('/clubs/sb', clubsController.getStudentBodies);

// router.get('/clubs/search-clubs', clubsController.getSearchClubs);

// router.get('/clubs/getSearchIndex', clubsController.getClubsSearchIndex);

// router.get('/clubs/events', clubsController.getEvents);

// router.get('/clubs/:clubid', clubsController.getClub);

// router.get('/clubs/events/eventnames', clubsController.listEvents);

// router.get('/clubs/events/searchq', clubsController.searchEvents);

// router.get('/clubs/events/list', clubsController.listEventNames);

// router.get('/clubs/events/sortq', clubsController.sortEvents);

// router.get('/clubs/events/latest', eventsController.getLatestEvents);

// router.get('/clubs/events/:eventid', clubsController.getEvent);

// router.post('/clubs/events/register/:eventid',[
//   check('email').isEmail().withMessage('Email must be a proper email'),
//   check('name').isLength({ min: 5 }).withMessage('Name must be minimum 5 characters'),
//   check('college').isLength({ min: 2 }).withMessage('Please Enter proper college Name'),
//   check('cnum').isLength({min:9}).withMessage('Please enter a proper mobile number'),
//   check('reg').isLength({min:4}).withMessage('Please enter a proper registration number/college id'),
// ],captcha, clubsController.postRegisterEvent);




// //Payment details

// router.get('/payment', eventsController.makePayments);
// router.get('/paytmCallback', eventsController.getPaytmCallback);
// router.post('/paytmCallback', eventsController.postPaytmCallback);
// router.post('/getTxnStatus', eventsController.getTransactionStatus);
// router.post('/updatePaymentStatus', eventsController.updatePaymentStatus);

module.exports = router;