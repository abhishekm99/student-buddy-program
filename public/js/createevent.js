// function makeTimer() {

// 	//		var endTime = new Date("29 April 2018 9:56:00 GMT+01:00");	
// 		var endTime = new Date("09 February 2020 5:15:00 GMT+05:30");			
// 			endTime = (Date.parse(endTime) / 1000);

// 			var now = new Date();
// 			now = (Date.parse(now) / 1000);

// 			var timeLeft = endTime - now;

// 			var months = Math.floor((timeLeft / 86400)/30);
// 			var days = Math.floor(timeLeft / 86400);
// 			var hours = Math.floor((timeLeft - (days * 86400)) / 3600);
// 			var minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600 )) / 60);
  
// 			var days = Math.floor((timeLeft / 86400)%30);
// 			if (hours < "10") { hours = "0" + hours; }
// 			if (minutes < "10") { minutes = "0" + minutes; }
// 			if (seconds < "10") { seconds = "0" + seconds; }

// 			$("#days").html(months + "<span class='reg-span'>Months</span>");
// 			$("#hours").html(days + "<span class='reg-span'>Days</span>");
// 			$("#minutes").html(hours + "<span class='reg-span'>Hours</span>");
// 			$("#seconds").html(minutes + "<span class='reg-span'>Minutes</span>");		

// 	}

// 	setInterval(function() { makeTimer(); }, 1000);

var $input;

function onInputFocus(event) {
  var $target = $(event.target);
  var $parent = $target.parent();
  $parent.addClass('input--filled');
};

function onInputBlur(event) {
  var $target = $(event.target);
  var $parent = $target.parent();

  if (event.target.value.trim() === '') {
    $parent.removeClass('input--filled');
  }
};

$(document).ready(function() {
  $input = $('.input__field');
  
  // in case there is any value already
  $input.each(function(){
    if ($input.val().trim() !== '') {
      var $parent = $input.parent();
      $parent.addClass('input--filled');
    }
  });
  
  $input.on('focus', onInputFocus);
  $input.on('blur', onInputBlur);
});
    