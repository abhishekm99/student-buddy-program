$(document).ready(function(){
    //Search autocomplete method
    const search = document.getElementById('ename');
    const matchList = document.getElementById('matchList');
    let array_data = [];
    $.get('/clubs/events/eventnames', (datas) => {
        for(i=0;i<datas.names.length;i++){
            array_data.push(datas.names[i]);
        }
    });
    const searchStates = searchText => {
        matchList.style.display = 'block';
        let matches = array_data.filter(data => {
            const regex = new RegExp(`${searchText}`, 'gi');
            return data.match(regex);
        });
        if(searchText.length == 0){
            matches = [];
            matchList.innerHTML = '';
        }
        outputHtml(matches);
    }

    const outputHtml = matches => {
        let html;
        if(matches.length>0){
            html = matches.map(match => `
                <li class="card-body mb-1 named" data-value="${match}">
                ${match}
                </li>
            `).join('');
            matchList.innerHTML = html;
            
        }
        else if(search.value.length===0){
            matchList.innerHTML = '';
            matchList.style.display = 'none';
        }
        else{
            html = `
            <span class="card-body mb-1">No results found</span>
        `; 
        matchList.innerHTML = html;
        }
        
        $('.named').click(function() {
            var d = $(this).data('value');      
            search.value = d;
            matchList.innerHTML = '';
            matchList.style.display = 'none';
        });
    }

    search.addEventListener('input', () => searchStates(search.value))
})