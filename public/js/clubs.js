$(document).ready(function() {
    (function($) {
        $.fn.timeline = function() {
          var selectors = {
            id: $(this),
            item: $(this).find(".timeline-item"),
            activeClass: "timeline-item--active",
            img: ".timeline__img"
          };
          selectors.item.eq(0).addClass(selectors.activeClass);
          selectors.id.css({
            "background-image":"linear-gradient(0deg,rgba(0, 0, 0, 0.6),rgba(0, 0, 0, 0.6)), url(" +
              selectors.item
                .first()
                .find(selectors.img)
                .attr("data-src") +
              ")",
              "background-size":"cover"
          });
          var itemLength = selectors.item.length;
          $(window).scroll(function() {
            var max, min;
            var pos = $(this).scrollTop();
            selectors.item.each(function(i) {
              min = $(this).offset().top - 100;
              max = $(this).height() + $(this).offset().top;
              var that = $(this);
              if (i == itemLength - 2 && pos > min + $(this).height() / 2) {
                selectors.item.removeClass(selectors.activeClass);
                selectors.id.css({
                  "background-image":"linear-gradient(0deg,rgba(0, 0, 0, 0.6),rgba(0, 0, 0, 0.6)), url(" +
                    selectors.item
                      .last()
                      .find(selectors.img)
                      .attr("data-src") +
                    ")",
                  "background-size":"cover"
                });
                selectors.item.last().addClass(selectors.activeClass);
              } else if (pos <= max - 40 && pos >= min) {
                selectors.id.css(
                  {
                    "background-image":"linear-gradient(0deg,rgba(0, 0, 0, 0.6),rgba(0, 0, 0, 0.6)), url(" +
                      $(this)
                        .find(selectors.img)
                        .attr('data-src') +
                      ")",
                      "background-size":"cover"
                  });
                selectors.item.removeClass(selectors.activeClass);
                $(this).addClass(selectors.activeClass);
              }
            });
          });
        };
      })(jQuery);
    $("#timeline-1").timeline();


    let lazyImages = [...document.querySelectorAll('.lazy-image')];
    let inAdvance = 300;

    function lazyLoad() {
      lazyImages.forEach(image => {
        if (image.offsetTop < window.innerHeight + window.pageYOffset + inAdvance) {
          image.src = image.dataset.src;
          image.onload = () => image.classList.add('loaded');
        }
      });

      // if all loaded removeEventListener
    }

    lazyLoad();

    window.addEventListener('scroll', _.throttle(lazyLoad, 16));
    window.addEventListener('resize', _.throttle(lazyLoad, 16));
});