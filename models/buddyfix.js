const Sequelize = require('sequelize');
const { models } = require('../helpers/database');

const sequelize = require('../helpers/database');

const Buddyfix = sequelize.define('buddyfix', {
    id: {
        type:Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    contactnumber: {
        type: Sequelize.STRING,
        allowNull: false
    },
    userId: {
        type:Sequelize.INTEGER,
        allowNull: false,
    },
},
{
    timestamps: false,
});

module.exports = Buddyfix;