const Sequelize = require('sequelize');

const sequelize = require('../helpers/database');

const Interest = sequelize.define('Interests', {
    id: {
        type:Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    Interests: {
        type:Sequelize.TEXT,
        allowNull: false,
    },
    Category: {
        type:Sequelize.TEXT,
        allowNull: false,
    }
},
{
    timestamps: false,
})

module.exports = Interest;