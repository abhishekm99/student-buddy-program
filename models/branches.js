const Sequelize = require('sequelize');

const sequelize = require('../helpers/database');

const Branch = sequelize.define('Branch', {
    id: {
        type:Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    branchName: {
        type:Sequelize.STRING,
        allowNull: false,
    },
},
{
    timestamps: false,
})

module.exports = Branch;