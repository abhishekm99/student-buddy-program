const Sequelize = require('sequelize');

const sequelize = require('../helpers/database');

const Notification = sequelize.define('notification', {
    id:{
        type:Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    userid:{
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    notificationtext:{
        type: Sequelize.STRING,
        allowNull: false,
    },
    readToken:{
        type: Sequelize.BOOLEAN,
        allowNull: false,
    }
},
{
    timestamps: true,
})

module.exports = Notification;