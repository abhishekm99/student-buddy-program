const Sequelize = require('sequelize');

const sequelize = require('../helpers/database');

const Request = sequelize.define('Request', {
    id: {
        type:Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    buddyId: {
        type:Sequelize.INTEGER,
        allowNull: false,
    },
    budId: {
        type:Sequelize.INTEGER,
        allowNull: false,
    },
    permission: {
        type:Sequelize.BOOLEAN,
        default: 0,
        allowNull: false,
    }
},
{
    timestamps: true,
})

module.exports = Request;