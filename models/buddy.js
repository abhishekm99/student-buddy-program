const Sequelize = require('sequelize');
const { models } = require('../helpers/database');

const sequelize = require('../helpers/database');

const Buddy = sequelize.define('buddy', {
    id: {
        type:Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    email: {
        type: Sequelize.STRING,
        allowNull: false
    },
    contactnumber: {
        type: Sequelize.STRING,
        allowNull: false
    },
    gender:{
        type:Sequelize.STRING,
        allowNull:true
    },
    reg:{
        type:Sequelize.STRING,
        allowNull:true
    },
    branch:{
        type:Sequelize.STRING,
        allowNull:true
    },
    interests:{
        type:Sequelize.TEXT,
        allowNull:true
    },
    budCount: {
        type: Sequelize.INTEGER,
        default: 0,
        allowNull: false,
    }
},
{
    timestamps: true,
});

module.exports = Buddy;