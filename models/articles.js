const Sequelize = require('sequelize');

const sequelize = require('../helpers/database');

const Article = sequelize.define('article', {
    id: {
        type:Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    articleName: {
        type:Sequelize.STRING,
        allowNull: false,
    },
    imageUrl:{
        type:Sequelize.STRING,
        allowNull: false,
    },
    tags:{
        type: Sequelize.TEXT,
        allowNull: false,
    },
    shortDesc:{
        type: Sequelize.TEXT,
        allowNull: false,
    },
    articleUrl: {
        type:Sequelize.STRING,
        allowNull: false,
    }

},
{
    timestamps: false,
})

module.exports = Article;