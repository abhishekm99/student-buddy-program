const Sequelize = require('sequelize');
const { models } = require('../helpers/database');

const sequelize = require('../helpers/database');

const User = sequelize.define('user', {
    id: {
        type:Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    reg:{
        type: Sequelize.STRING,
        allowNull: false
    },
    email: {
        type: Sequelize.STRING,
        allowNull: false
    },
    password:{
        type:Sequelize.STRING,
        allowNull:true
    },
    utype:{
        type:Sequelize.STRING,
        default: 'bud',
        allowNull:false,
    },
    emailVerification:{
        type:Sequelize.BOOLEAN,
        default: 0,
        allowNull: false,
    },
    emailToken: Sequelize.STRING,
    resetToken: Sequelize.STRING,
    resetTokenExpiration: Sequelize.DATE,
},
{
    timestamps: true,
});

module.exports = User;