const Sequelize = require('sequelize');
const { models } = require('../helpers/database');

const sequelize = require('../helpers/database');

const Bud = sequelize.define('bud', {
    id: {
        type:Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    email: {
        type: Sequelize.STRING,
        allowNull: false
    },
    contactnumber: {
        type: Sequelize.STRING,
        allowNull: false
    },
    gender:{
        type:Sequelize.STRING,
        allowNull:true
    },
    branch:{
        type:Sequelize.STRING,
        allowNull:true
    },
    reg:{
        type:Sequelize.STRING,
        allowNull:true
    },
    interests:{
        type:Sequelize.TEXT,
        allowNull:true
    },
    buddyname:{
        type:Sequelize.STRING,
        allowNull:true
    },
    buddyemail:{
        type:Sequelize.STRING,
        allowNull:true
    },
    buddycontact:{
        type:Sequelize.STRING,
        allowNull:true
    },
},
{
    timestamps: true,
});

module.exports = Bud;