const Sequelize = require('sequelize');

const sequelize = require('../helpers/database');

const Message = sequelize.define('Message', {
    id: {
        type:Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    senderName: {
        type:Sequelize.STRING,
        allowNull: false,
    },
    senderUserId:{
        type:Sequelize.INTEGER,
        allowNull: false,
    },
    buddyId: {
        type:Sequelize.INTEGER,
        allowNull: false,
    },
    budId: {
        type:Sequelize.INTEGER,
        allowNull: false,
    },
    message: {
        type:Sequelize.TEXT,
        allowNull: false,
    }
},
{
    timestamps: true,
})

module.exports = Message;