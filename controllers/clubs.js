
const mysql = require('mysql');
// const Club = require('../models/club');
// const Events = require('../models/events');
const dotenv = require('dotenv');
const sequelize = require('../helpers/database');
const Sequelize = require('sequelize');
const { search } = require('../app');
const { query } = require('express');
const Op = Sequelize.Op;
// const Participant = require('../models/participant');
const uuidv4 = require('uuid/v4');
// const Paytm = require('../models/paytm');s
const nodemailer = require('nodemailer')
const { check, validationResult } = require('express-validator');
const awsconfig = require('../config/config');

const aws = require('aws-sdk');

aws.config.update(awsconfig);

let transporter = nodemailer.createTransport({
    SES: new aws.SES({
        apiVersion: '2010-12-01'
    })
});

dotenv.config();
const db = mysql.createConnection ({
    host: 'localhost',
    user: `${process.env.MYSQL_USER}`,
    password: `${process.env.MYSQL_PASS}`,
    database: `${process.env.MYSQL_DB}`,

    options: {
        useColumnNames: true,
    }
  });
  try{
  db.connect((err) => {
    if (err) {
        throw err;
    }
  });
  global.db = db;
  }
  catch(err) {
    console.log(err);
} 

exports.getClubs = (req,res,next) => {
    Club.findAll().then(products => {
        res.render('clubs/clubs', {items: products,notMain: true, title:'Clubs'});
    })
    
}

exports.getTechnicalClubs = (req,res,next) => {
    page = req.query.p;
    limit = 5;
    if(!page){
        page=1;
    }
    offset = (page*limit)-limit;
    Club.findAndCountAll({where:{ClubType:'Technical', approved:true},limit:limit,offset:offset,order:[['Name','ASC']]}).then(products => {
        pages = Math.ceil(products.count/limit);
        res.render('clubs/technical', {items: products.rows,notMain: true, title:'Technical Clubs',pages:pages, currentPage: page});
    })
}

exports.getCulturalClubs = (req,res,next) => {
    page = req.query.p;
    limit = 5;
    if(!page){
        page=1;
    }
    offset = (page*limit)-limit;
    Club.findAndCountAll({where:{ClubType:'Cultural', approved:true},limit:limit,offset:offset,order:[['Name','ASC']]}).then(products => {
        pages = Math.ceil(products.count/limit);
        res.render('clubs/cultural', {items: products.rows,notMain: true, title:'Cultural Clubs',pages:pages, currentPage: page});
    })
}

exports.getSportsClubs = (req,res,next) => {
    page = req.query.p;
    limit = 5;
    if(!page){
        page=1;
    }
    offset = (page*limit)-limit;
    Club.findAndCountAll({where:{ClubType:'Sports', approved:true},limit:limit,offset:offset,order:[['Name','ASC']]}).then(products => {
        pages = Math.ceil(products.count/limit);
        res.render('clubs/sports', {items: products.rows,notMain: true, title:'Sports Clubs',pages:pages, currentPage: page});
    })
}

exports.getOtherClubs = (req,res,next) => {
    page = req.query.p;
    limit = 5;
    if(!page){
        page=1;
    }
    offset = (page*limit)-limit;
    Club.findAndCountAll({where:{ClubType:'Student Projects', approved:true},limit:limit,offset:offset,order:[['Name','ASC']]}).then(products => {
        pages = Math.ceil(products.count/limit);
        res.render('clubs/others', {items: products.rows,notMain: true, title:'Student Projects',pages:pages, currentPage: page});
    })
}

exports.getStudentBodies = (req,res,next) => {
    page = req.query.p;
    limit = 5;
    if(!page){
        page=1;
    }
    offset = (page*limit)-limit;
    Club.findAndCountAll({where:{ClubType:'Student Body', approved:true},limit:limit,offset:offset,order:[['id','DESC']]}).then(products => {
        pages = Math.ceil(products.count/limit);
        res.render('clubs/sb', {items: products.rows,notMain: true, title:'Student Body',pages:pages, currentPage: page});
    })
}

exports.getSearchClubs = (req,res,next) => {
    let { searchq } = req.query;
    if(!searchq || searchq===''){
        page = req.query.p;
        limit = 5;
        if(!page){
            page=1;
        }
        offset = (page*limit)-limit;
        Club.findAndCountAll({where:{approved:true},limit:limit,offset:offset,order:[['Name','ASC']]}).then(products => {
            pages = Math.ceil(products.count/limit);
            res.render('clubs/all-search', {items: products.rows,notMain: true, title:'Search',pages:pages,hasitems: products.rows.length>0, currentPage: page});
        })
    }
    else
    {
        let query = 'select * from clubs WHERE concat(Name,Tags) like ? and approved = true';
            db.query(query, '%' + searchq + '%', (err, result)=> {
                if(err) throw err;
                else{
                    products=result;
                    res.render('clubs/all-search', {
                        items:products,
                        hasitems: products.length>0,
                        title: 'Clubs Search',
                        notMain: true,
                    })
                }
            })
    }
}

exports.getClubsSearchIndex = (req,res,next) => {
    if(req.query.token==="gkXjW4Dnjt0m2Steio3DwFdRVlmn0JCfXIETR68PlqwDEjqCv1E8kpQSVYiKD1JC"){
        let query = 'select Tags,Name from clubs where approved=true';
        db.query(query, (err,result) => {
            indexarray = [];
            result.map(data => {
                tags = data.Tags.split(',');
                for(i=0;i<tags.length;i++){
                    indexarray.push(tags[i]);
                }
                indexarray.push(data.Name);
            });
            for(i=0;i<indexarray.length;i++){
                indexarray[i] = indexarray[i].trim();
            }

            let uniqueArray = [];
                // Loop through array values
            for(i=0; i < indexarray.length; i++){
                if(uniqueArray.indexOf(indexarray[i]) === -1) {
                    uniqueArray.push(indexarray[i]);
                }
            }

            indexarray = uniqueArray;
            for(i=1;i<indexarray.length;i++){
                for(j=0;j<i;j++){
                    if(indexarray[i].toUpperCase().localeCompare(indexarray[j].toUpperCase())==0){
                        indexarray.splice(i,1);
                    }
                    else if(indexarray[i]==="" || indexarray[i]===" "){
                        indexarray.splice(i,1); 
                    }
                }
            }
            return res.send(indexarray);
        })
    }
    else{
        return res.json({
            message:'Access denied'
        });
    }
}

exports.getClub = (req,res,next) => {
    let clubid = req.params.clubid;
    Club.findByPk(clubid).then(club => {
        res.render('clubs/clubpage', {club: club, title: club.Name,notMain: true, desc: club.Description})
    })
}

exports.getEvents = (req,res,next) => {
    page = req.query.p;
    limit = 12;
    if(!page){
        page=1;
    }
    offset = (page*limit)-limit;
    let today = new Date();
    today = today.toISOString().slice(0,10);
    Events.findAndCountAll({where:{eventRegToken:1, [Op.or]: {eventdate: {[Op.gte]: today},eventenddate: {[Op.gte]: today}}},limit:limit,offset:offset,order:[['eventdate','ASC']]}).then(events => {
        pages = Math.ceil(events.count/limit);
        res.render('events/events', {items: events.rows,errors: req.flash('error'),success: req.flash('success'), title:'Events',pages:pages, currentPage: page, notMain: true});
    })
}

exports.searchEvents = (req,res,next) => {
    namequery = req.query.searchn;
    typequery = req.query.searcht;
    datequery = req.query.searchd;
    if(!namequery){
        namequery = 'undefined'
    }
    if(!typequery){
        typequery = 'undefined'
    }
    if(!datequery){
        console.log('No date query')
        Events.findAndCountAll({where:{[Op.or]: [
            { eventname: { [Op.like]: `%${namequery}%`} },
            { eventtags: { [Op.like]: `%${typequery}%`} },
          ],eventRegToken:1},order:[['eventdate','ASC']]}).then(events => {
            res.render('events/events', {items: events.rows, title:'Events', notMain: true});
        })
    }else{
        Events.findAndCountAll({where:{[Op.or]: [
            { eventname: { [Op.like]: `%${namequery}%`} },
            { eventtags: { [Op.like]: `%${typequery}%`} },
            { eventdate: {[Op.gte]: datequery} },
          ],eventRegToken:1},order:[['eventdate','ASC']]}).then(events => {
            res.render('events/events', {items: events.rows, title:'Events', notMain: true});
        })
    }    
}

exports.sortEvents = (req,res,next) => {
    eventtype = req.query.sortt;
    clubname= req.query.sortn;
    if(!clubname){
        Events.findAndCountAll({where:{[Op.or]: [
            { eventtags: { [Op.like]: `%${eventtype}%`} },
          ], eventRegToken:1},order:[['eventdate','ASC']]}).then(events => {
            res.render('events/events', {items: events.rows, title:'Events', notMain: true});
        })
    }else if(!eventtype){
        Events.findAndCountAll({where:{[Op.or]: [
        { clubname: { [Op.like]: `%${clubname}%`} },
        ], eventRegToken:1},order:[['eventdate','ASC']]}).then(events => {
        res.render('events/events', {items: events.rows, title:'Events', notMain: true});
        })
    }
    else{
        res.redirect('/clubs/events');
    }
    
}


exports.listEvents = (req,res,next) => {
    Events.findAll({where:{eventRegToken: 1, eventApproval: 1}}).then(result => {
        namesarray = [];
        result.map(data => {
                namesarray.push(data.eventname);
            });
        for(i=0;i<namesarray.length;i++){
            namesarray[i] = namesarray[i].trim();
        }
        let uniquenamesArray = [];
        // Loop through array values
        for(i=0; i < namesarray.length; i++){
            if(uniquenamesArray.indexOf(namesarray[i]) === -1) {
                uniquenamesArray.push(namesarray[i]);
            }
        }
        namesarray = uniquenamesArray;
        for(i=1;i<namesarray.length;i++){
            for(j=0;j<i;j++){
                if(namesarray[i].toUpperCase().localeCompare(namesarray[j].toUpperCase())==0){
                    namesarray.splice(i,1);
                }
                else if(namesarray[i]==="" || namesarray[i]===" "){
                    namesarray.splice(i,1); 
                }
            }
        }
        res.json({
            names: namesarray
        });
    })
}

exports.listEventNames = (req,res,next) => {
    let today = new Date();
    today = today.toISOString().slice(0,10);
    Events.findAll({where:{eventRegToken: 1, eventApproval: 1, [Op.or]: {eventdate: {[Op.gte]: today},eventenddate: {[Op.gte]: today}}}}).then(result => {
        tagsarray = [];
        namesarray = [];
        result.map(data => {
            let tags = data.eventtags.split(',');
            for(i=0;i<tags.length;i++){
                if(tags[i].length>0){
                    tagsarray.push(tags[i]);
                }
            }
            namesarray.push(data.clubname);
        });
        for(i=0;i<tagsarray.length;i++){
            tagsarray[i] = tagsarray[i].trim();
        }
        for(i=0;i<namesarray.length;i++){
            namesarray[i] = namesarray[i].trim();
        }
        let uniquetagsArray = [];
        let uniquenamesArray = [];
            // Loop through array values
        for(i=0; i < tagsarray.length; i++){
            if(uniquetagsArray.indexOf(tagsarray[i]) === -1) {
                uniquetagsArray.push(tagsarray[i]);
            }
        }
        for(i=0; i < namesarray.length; i++){
            if(uniquenamesArray.indexOf(namesarray[i]) === -1) {
                uniquenamesArray.push(namesarray[i]);
            }
        }

        tagsarray = uniquetagsArray;
        for(i=1;i<tagsarray.length;i++){
            for(j=0;j<i;j++){
                if(tagsarray[i].toUpperCase().localeCompare(tagsarray[j].toUpperCase())==0){
                    tagsarray.splice(i,1);
                    break;
                }
                else if(tagsarray[i]==="" || tagsarray[i]===" "){
                    tagsarray.splice(i,1); 
                }
            }
        }

        namesarray = uniquenamesArray;
        for(i=1;i<namesarray.length;i++){
            for(j=0;j<i;j++){
                if(namesarray[i].toUpperCase().localeCompare(namesarray[j].toUpperCase())==0){
                    namesarray.splice(i,1);
                }
                else if(namesarray[i]==="" || namesarray[i]===" "){
                    namesarray.splice(i,1); 
                }
            }
        }
        res.json({
            tags: tagsarray,
            names: namesarray
        });
    })
}

exports.getEvent = (req,res,next) => {
    eid = req.params.eventid;
    let ended;
    let today = new Date();
    Events.findByPk(eid).then(event => {
        if(event.dataValues.eventdate <= today.toISOString().slice(0,10) || event.dataValues.eventRegToken === false){
            ended = true;
        }
        else{
            ended = false;
        }
        let formdata = req.flash('formdata')[0];
        res.render('events/eventpage', {title: event.dataValues.eventname ,error: req.flash('error'), desc: event.dataValues.eventdesc, formdata: formdata, notMain: true, event: event, ended: ended})
    })
}

exports.postRegisterEvent = (req,res,next) => {
    eid = req.params.eventid;
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        var formdata = {
            name: req.body.name,
            reg: req.body.reg,
            cnum: req.body.cnum,
            wnum: req.body.wnum,
            email: req.body.email,
        }
        req.flash('error', errors.array());
        req.flash('formdata', formdata)
        req.session.save(function () {
            res.redirect(`/clubs/events/${eid}`);
        });
    }
    else{
        Events.findByPk(eid).then(event => {
            if(event.eventRegToken != 1){
                res.redirect('/clubs/events');
            }else{
                let name = req.body.name;
                let reg = req.body.reg;
                let college = req.body.college;
                let cnum = req.body.cnum;
                let wnum = req.body.wnum;
                let email = req.body.email;
                let oid;
                let amount = event.eventPrice;
                oid = uuidv4();
                Participant.create({Name: name, Registration: reg,College: college, Email: email, WhatsappNum: wnum, ContactNum: cnum, Email: email, eventid: event.id, eventname: event.eventname, clubid: event.clubid, clubname: event.clubname, paymentStatus: 0, amountpaid: amount, orderid: oid}).then(result => {
                    // console.log(result.dataValues.Email);
                    transporter.sendMail({
                        to:result.dataValues.Email,
                        from:'Events Portal <noreply.events@mitsc.in>',
                        subject: `Registered for Event ${result.dataValues.eventname}`,
                        html:`
                        <div class="container">
                            <div class="top" style="height:100px; background-color: #f2f2f2;">
                                <center>
                                    <h2>Events Portal MIT, Manipal</h2>
                                </center>
                            </div>
                            <div class="midd" style="height:300px">
                                <center>
                                    <h3 style="font-style: italic;color: #919191;">Hello from Events Portal!</h3>
                                    <h5 style="font-style: italic;color: #919191;">Dear ${result.dataValues.Name}, </h5>
                                    <h1 style="font-weight: bold;">You have been registered for ${result.dataValues.eventname} Conducted by ${result.dataValues.clubname}</h1>
                                    <p>You will be contacted by the club regarding the same soon, in case you would like to get in touch</p>
                                    <p>with the club then please visit the page https://clubs.mitportals.in/clubs/${result.dataValues.clubid}</p>
                                    <p>You can contact the event coordinator ${event.eventcontactname} at ${event.eventcontact} for any further queries </p>
                                    <p>We hope to see you register for more events</p>
                                    <p>For any queries please email us at mitsc20@gmail.com (Student Council, MIT)</p>
                                </center>
                            </div>
                        </div>
                        `,
                    });
                    return result;
                }).then(result => {
                    if(result.dataValues.amountpaid == 0){
                        req.flash('success', `You have been registered, please check your email for more details`);
                        req.session.save(function () {
                            res.redirect('/clubs/events');
                        });
                    }
                    else{
                        Paytm.create({
                            Order_Id: oid,
                            Cust_Id: result.dataValues.id,
                            Cust_Name: result.dataValues.Name,
                            Event_Id: result.dataValues.eventid,
                            Event_Name: result.dataValues.eventname,
                            Amount: result.dataValues.amountpaid,
                            Contact: result.dataValues.ContactNum,
                            Email: result.dataValues.Email
                        }).then(result => {
                            res.redirect(`/payment?orderid=${oid}&callback=http://localhost:4001/paytmCallback`)
                        })
                    }   
                })
            }
        })
    }
}