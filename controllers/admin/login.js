const bcrypt = require('bcryptjs');
const crypto = require('crypto');
const User = require('../../models/users');
const Buds = require('../../models/bud');
const Buddy = require('../../models/buddy');
const Buddyfix = require('../../models/buddyfix');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const fs = require('fs');
const { validationResult } = require('express-validator/check');
const { where } = require('sequelize');
const multer = require("multer");
const { ECONNRESET } = require('constants');
const { createAnnouncement, postNotification } = require('../events');
const { match_interests, match_interests_withoutBranch } = require('../buddy');
// const Participant = require('../../models/participant');
const Notification = require('../../models/notifications');
const mailing = require('../../helpers/email');

const aws = require('aws-sdk');
const { sendEmail } = require('../../helpers/email');
const { mailer } = require('../../config/paytm-meta');
const Bud = require('../../models/bud');
const Interests = require('../../models/interests');

exports.getLogin = (req,res,next) => {
    if(!req.session.isLoggedIn){
        res.render('admin/login', {
            pageTitle: 'Login',
            isAuthenticated: req.session.isLoggedIn,
            error: req.flash('error'),
            title:'Login',
            notMain: true,
            success: req.flash('success'),
        });
    }else{
        res.redirect('/admin/dashboard')
    }
}

exports.postLogin = (req,res,next) => {
    const email = req.body.clubemail;
    const password = req.body.pass;
    // res.json({
    //     success: false,
    //     msg: "Sorry, Register and Login has been temporarily suspended as the registration dates for the program has been changed"
    // })
    User.findOne({where: {email: email}})
        .then(user => {
            if(!user){
                res.json({
                    success: false,
                    msg: "Sorry, no such user exists"
                })
            }
            bcrypt.compare(password, user.password).then(result => {
                if(result){
                    if(user.emailVerification === true){
                        req.session.user = user;
                        req.session.isLoggedIn = true;                        
                        return req.session.save(err => {
                            res.json({
                                success: true,
                            })
                        })
                    }else{
                        res.json({
                            success: false,
                            unverified: true,
                            msg: "Sorry, Please verify your email address before continuing. If you did not recieve an email, please click on the resend verification button below"
                        })
                    }
                }
                else{ 
                    res.json({
                        success: false,
                        msg: "Sorry, Invalid email or password"
                    })
                }
            }).catch(err => 
            {
                console.log(err);
                req.session.save(function () {
                    return res.redirect(`/admin/login`);
                });
            })
        })
    .catch(err => console.log(err));
}

exports.getRegister = (req,res,next) => {
            res.render('admin/register', {
                pageTitle: "Sign Up",
                isAuthenticated: req.session.isLoggedIn,
                error: req.flash('error'),
                title: 'Register',
                notMain: true,
            })
}

exports.postRegister = (req,res,next) => {
    let name = req.body.name;
    let email = req.body.email;
    let password = req.body.password;
    let cnfpassword = req.body.cnfpassword;
    let reg = req.body.reg;
    let type = reg.slice(0,3);
    let regslice = reg.slice(0,3);
    if(type === '120' || type === '200' || type === '190' || type === '220'){
        type = 'bud';
    }else{
        type = 'buddy';
    }

    if(regslice === '120' || regslice === '190' || regslice === '200' || regslice === '220'){
        User.findOne({where: {email: email}}).then(user => {
            if(user){
                res.json({
                    success: false,
                    msg: "Sorry, User with this email already exists. Please try logging in or resetting your password"
                })
            }else{
                crypto.randomBytes(32, (err, buffer) => {
                    if(err){
                        console.log(err);
                        return res.redirect('/admin/login');
                    }else{
                        const token = buffer.toString('hex');
                        if(password === cnfpassword){
                            bcrypt.hash(password, 12)
                                .then(hashedpassword => {
                                    const user = User.create({
                                    name:name,
                                    reg: reg,
                                    email: email,
                                    password:hashedpassword,                            
                                    utype:type,
                                    emailToken: token,
                                    emailVerification: 0,
                                    createdAt: new Date(),
                                    updatedAt: new Date(),
                                }).then(result => {
                                    let body = `
                                            <center>
                                                <h1 style="font-style: italic;color: #919191;">Hello from MITSC!</h1>
                                                <h2>Dear ${name},</h2>
                                                <h3 style="font-weight: bold;">To Confirm your email please visit the following link</h3>
                                                <p>If you did not register for the program then please inform us at mitsc20@gmail.com</p>
                                                <a href="https://studentbuddy.mitportals.in/admin/confirmemail?email=${email}&token=${token}">Click here to confirm your email</a>
                                            </center>
                                    `
                                    sendEmail(email, `Student Buddy Program Email Verification ${name}`, body);
                                    res.json({
                                        success: true,
                                        msg: "Thank you for registering, please check your email for further verification details. For Gmail users, please check the promotions/updates tab as well if you cannot find the email. You will need to login after verification to complete registration"
                                    })
                                })
                            })
                        }
                        else{
                            res.json({
                                success: false,
                                msg: "Sorry, The two passwords do not match, please check the passwords"
                            })
                        }           
            
                    }
                })    
            }
        })
    }else{
        res.json({
            success: false,
            msg: "Sorry, Registration are only open for freshers/lateral entry students"
        })
    }
    
}

exports.verifyEmail = (req,res,next) => {
    let email = req.query.email;
    let token = req.query.token;
    User.findOne({where: {email: email}}).then(user => {
        if(user.emailToken === token){
            user.emailVerification = 1;
            user.emailToken = null;
            user.save().then(result => {
                req.flash('success', 'Thank you for verifying, please login to complete registration process');
                req.session.save(function () {
                    return res.redirect(`/admin/login`);
                });
            })
        }else{
            req.flash('error', 'Sorry, the token did not match please try again later');
            req.session.save(function () {
                return res.redirect(`/admin/login`);
            });
        }
    }).catch(err => {
        req.flash('error', 'Sorry, the token did not match or email did not match');
        req.session.save(function () {
            return res.redirect(`/admin/login`);
        });
    })
}

exports.postLogout = (req,res,next) => {
    req.session.destroy(err=> {
        console.log(err);
        res.redirect('/admin/login')
    });
}


exports.getDashboard = (req,res,next) => {
    console.log(req.user.utype);
        if(req.user.utype === 'bud'){
                Buds.findOne({where: {userId: req.user.id}}).then(bud => {
                    if(!bud){
                        filleddata = {
                            name: req.user.name,
                            email: req.user.email,
                            reg: req.user.reg,
                            type: 'Bud'
                        }
                        res.render('admin/dashboard', {title: 'Dashboard', notMain: true, notRegistered: true, data: filleddata, error: req.flash('error'),success: req.flash('success'), nobodyinbranch: req.flash('nobodyinbranch')})
                    }else{
                        Buddy.findOne({where:{id: bud.buddyId}}).then(buddy => {
                            filleddata = {
                                name: req.user.name,
                                email: req.user.email,
                                reg: req.user.reg,
                                type: 'Bud',
                                budinterests: bud.interests.split(','),
                                buddyinterests: buddy.interests.split(','),
                            }
                            res.render('admin/dashboard', {title: 'Dashboard', notMain: true, notRegistered: false, data: filleddata, buddy: buddy, bud: bud, error: req.flash('error'),success: req.flash('success')})
                        })
                    }
            }).catch(err => console.log(err));
        }
        else if(req.user.utype === 'buddy'){
            Buddy.findOne({where: {userId: req.user.id}}).then(buddy => {
                    if(!buddy){
                        filleddata = {
                            name: req.user.name,
                            email: req.user.email,
                            reg: req.user.reg,
                            type: 'Buddy'
                        }
                        res.render('admin/buddydashboard', {title: 'Dashboard', notMain: true, notRegistered: true, data: filleddata, error: req.flash('error'),success: req.flash('success')})
                    }else{
                        Bud.findAll({where:{buddyId: buddy.id}}).then(buds => {
                            filleddata = {
                                name: req.user.name,
                                email: req.user.email,
                                reg: req.user.reg,
                                type: 'Buddy',
                                buddyinterests: buddy.interests.split(','),
                            }
                            res.render('admin/buddydashboard', {title: 'Dashboard', notMain: true, notRegistered: false, data: filleddata, buddy: buddy, buds: buds, error: req.flash('error'),success: req.flash('success')})
                        })
                    }
            }).catch(err => console.log(err));
        }
        else if(req.user.utype === 'council'){
            User.findAndCountAll({include:[Buddy,Bud]}).then(users => {
                // buds.rows.map(bud => {
                //     console.log(bud.dataValues.buddy.dataValues)
                // })
                // buddy = [];
                // users.rows.map(user => {
                //     buddy.push(user.buddy);
                // }).then(result => {
                //     res.json(buddy);
                // })
                res.render('admin/councildash', {title: 'Council Dashboard', notMain: true})
            })
        }
        else{
            res.redirect('/admin/login')
        }
}

exports.getFPwd = (req,res,next) => {
    res.render('admin/fpwd', {
        pageTitle: 'Reset Password',
        isAuthenticated: req.session.isLoggedIn,
        error: req.flash('error'),
        success: req.flash('success'),
        title:'Reset Password',
        notMain: true,
    });
}

exports.postReset = (req,res,next) => {
    crypto.randomBytes(32, (err, buffer) => {
        if(err){
            res.json({
                success: false,
                msg: "Sorry, Some error occured please try again later"
            })
        }
        const token = buffer.toString('hex');
        User.findOne({where:{email:req.body.clubemail}})
        .then(user => {
            if(!user){
                res.json({
                    success: false,
                    msg: "Sorry, No such account exists"
                })
            }
            user.resetToken = token;
            user.resetTokenExpiration = Date.now() + 3600000;
            return user.save();
        })
        .then(result => {
            var url;
            if(process.env.ENVIRONEMT === 'PRODUCTION'){
                url = 'https://studentbuddy.mitportals.in';
            }
            else{
                url = 'http://localhost:3009';
            }
            res.json({
                success: true,
                msg: "Please check your email for a password reset link"
            })
            let body = `
                    <center>
                        <h3 style="font-style: italic;color: #919191;">Student Buddy Program</h3>
                        <h1 style="font-weight: bold;">Password Reset Instructions</h1>
                        <p>Someone requested that the password be reset for your account</p>
                        <p>If this was a mistake, you are requested to bring it to the notice of the organising committee</p>
                        <p>To reset your password, visit the following address:</p>
                        <a href="https://studentbuddy.mitportals.in/admin/resetp/${token}">Click here to reset your password</a>
                    </center>
            `
            sendEmail(req.body.clubemail, 'Student Buddy Program Reset Password', body);
        }).then(result => {
            console.log(result);
        })
        .catch(err => console.log(err))
    })
}



exports.getNewPassword = (req,res,next) => {
    const token = req.params.token;
    User.findOne({where:{resetToken: token, resetTokenExpiration:{[Op.gt]: Date.now()}}})
    .then(user => {
        let message = req.flash('error');
        if(message.length>0){
            message = message[0];
        }
        else{
            message = null;
        }
        res.render('admin/rpwd', {
            pageTitle: 'Reset Password',
            error: message,
            userId: user.id,
            passwordToken: token,
            notMain: true
        })
    })
    .catch(err => {console.log(err)
        req.flash('error', 'Sorry, the token generated has either expired or is incorrect. Please generate another token');
        req.session.save(function () {
            return res.redirect(`/admin/login`);
        });
    });
}

exports.postNewPassword = (req,res,next) => {
    const newPassword = req.body.pass;
    const cnfpass = req.body.cnfpass;
    const userId = req.body.userId;
    const passwordToken = req.body.token;

    let resetUser;
    if(newPassword === cnfpass)
    {
        console.log(passwordToken,userId,cnfpass,newPassword)
        User.findOne({where:{resetToken: passwordToken, resetTokenExpiration:{[Op.gt]:Date.now()}, id:userId}})
        .then(user => {
            if(!user){
                res.json({
                    success: false,
                    msg: "Sorry, No such user exists"
                })
            }else{
                resetUser = user;
                return bcrypt.hash(newPassword,12);
            }
        })
        .then(hashedpassword => {
            resetUser.password = hashedpassword;
            resetUser.resetToken = null;
            resetUser.resetTokenExpiration = null;
            return resetUser.save();
        })
        .then(result => {
            res.json({
                success: true,
                msg: "Your Password has been reset, please wait for redirection to login"
            })
        })
        .catch(err => console.log(err));
    }else{
        res.json({
            success: false,
            msg: "Sorry, Two passwords do not match"
        })
    }
}


exports.getInterests = (req,res,next) => {
    Interests.findAll().then(interests => {
        res.json(interests);
    })
}

exports.registerProject = (req,res,next) => {
    var name = req.user.name;
    var email = req.user.email;
    var reg = req.user.reg;
    var gender = req.body.gender;
    var branch = req.body.branch;
    var contact = req.body.contact;
    var interests = req.body.interests_of_students;
    var type = req.body.type;
    console.log(name,email,reg,gender,branch,contact,interests,type)
    if(type === 'Buddy'){
        Buddy.create({
            name: name,
            email: email,
            contactnumber: contact,
            gender: gender,
            reg: reg,
            branch: branch,
            interests: interests,
            budCount: 0,
            createdAt: new Date(),
            updatedAt: new Date(),
            userId: req.user.id,
        }).then(result => {
            res.redirect('/admin/dashboard')
        })
    }else if(type === 'Bud'){
        var name = req.user.name;
        var email = req.user.email;
        var reg = req.user.reg;
        var gender = req.body.gender;
        var branch = req.body.branch;
        var contact = req.body.contact;
        var interests = req.body.interests_of_students;
        var type = req.body.type;
        Buds.findOne({where:{reg:reg}}).then(bud => {
            if(bud){
                res.json({
                    success: false,
                    msg: 'You are already registered, please refresh the page'
                })
            }else{
                let obj = match_interests(interests,gender,name,email,reg,contact,branch,req.user.id,res,req)
            }
        })
        
        // Buddy.findAll({where:{budCount:{[Op.lt]:2}}}).then(buddies => {
        //     if(buddies.length === 0){
        //         res.json({msg: 'Sorry, no buddies available'})
        //     }else{
        //         buddies.map(buddy => {
        //             if(buddy.budCount === 0 && buddy.branch === branch){
                        
        //                 .then(
        //                     res.render('buddy/buddy', {message: 'Congratulations your request has been submitted, please wait for the buddylist to be updated to know your buddy'})
        //                 )
        //             }
        //         })
        //     }
        // })
    }
}

exports.registerProjectWithoutBranch = (req,res,next) => {
    var name = req.user.name;
    var email = req.user.email;
    var reg = req.user.reg;
    var gender = req.body.gender;
    var branch = req.body.branch;
    var contact = req.body.contact;
    var interests = req.body.interests_of_students;
    var type = req.body.type;
    console.log(name,email,reg,gender,branch,contact,interests,type)
    if(type === 'Bud'){
        var name = req.user.name;
        var email = req.user.email;
        var reg = req.user.reg;
        var gender = req.body.gender;
        var branch = req.body.branch;
        var contact = req.body.contact;
        var interests = req.body.interests_of_students;
        var type = req.body.type;
        let obj = match_interests_withoutBranch(interests,gender,name,email,reg,contact,branch,req.user.id,res,req)
        // Buddy.findAll({where:{budCount:{[Op.lt]:2}}}).then(buddies => {
        //     if(buddies.length === 0){
        //         res.json({msg: 'Sorry, no buddies available'})
        //     }else{
        //         buddies.map(buddy => {
        //             if(buddy.budCount === 0 && buddy.branch === branch){
                        
        //                 .then(
        //                     res.render('buddy/buddy', {message: 'Congratulations your request has been submitted, please wait for the buddylist to be updated to know your buddy'})
        //                 )
        //             }
        //         })
        //     }
        // })
    }else{
        res.redirect('/admin/dashboard');
    }
}


exports.getNotification = (req,res,next) => {
    if(!req.session.isLoggedIn) {
        return res.json({
            msg: 'Loading Failed'
        })
    }
    else
    {
        Notification.findAndCountAll({where:{userid: req.user.id, readToken: 0}, order: [['createdAt','DESC']]}).then(notifications => {
            return res.json(notifications)
        })
    }
}

exports.getAllApplicants = (req,res,next) => {
    let type = req.query.type;
    console.log(type);
    if(req.user.utype === 'council'){
        if(type === 'buddy'){
            User.findAndCountAll({where:{utype: 'buddy'},include:[Buddy]}).then(users => {
                res.json(users);
            })
        }else{
            Bud.findAndCountAll({include:[Buddy]}).then(buds => {
                res.json(buds);
            })
        }
    }
    else{
        res.json({
            success: false,
            msg: 'Authentication failure'
        })
    }
}


exports.resendVerification = (req,res,next) => {
    let email = req.body.clubemail;
    if(email === ''){
        res.json({
            success: false,
            msg: 'Please provide email in the login box'
        })
    }else{
        User.findOne({where: {email: email}}).then(user => {
            if(!user){
                res.json({
                    success: false,
                    msg: "Sorry, User with this email does not exist"
                })
            }else{
                crypto.randomBytes(32, (err, buffer) => {
                    if(err){
                        console.log(err);
                        return res.redirect('/admin/login');
                    }else{
                        const token = buffer.toString('hex');
                        if(user.emailVerification === 1){
                            res.json({
                                success: false,
                                msg: "Sorry, User already verified"
                            })
                        }else{
                            user.emailToken = token;
                            user.save().then(result => {
                                let body = `
                                        <center>
                                            <h1 style="font-style: italic;color: #919191;">Hello from MITSC!</h1>
                                            <h2>Dear ${user.name},</h2>
                                            <h3 style="font-weight: bold;">To Confirm your email please visit the following link</h3>
                                            <p>If you did not register for the program then please inform us at mitsc20@gmail.com</p>
                                            <a href="https://studentbuddy.mitportals.in/admin/confirmemail?email=${email}&token=${token}">Click here to confirm your email</a>
                                        </center>
                                `
                                sendEmail(email, `Student Buddy Program Email Verification ${user.name}`, body);
                                res.json({
                                    success: true,
                                    msg: "A new Verification email has been sent, please check your email"
                                })
                            })
                        }
                    }
                })
            }
        })
    }
}

exports.getAboutManipal = (req,res,next) => {
    res.render('admin/aboutmanipal', {
        title: 'About Manipal',
    })
}

exports.councilbuddyfix = (req,res,next) => {
    Buddy.findAll().then(buddies => {
        buddies.map(buddy => {
            Buddyfix.findByPk(buddy.id).then(buddyfix => {
                buddy.contactnumber = buddyfix.contactnumber;
                buddy.save();
            })
        })
    })
}