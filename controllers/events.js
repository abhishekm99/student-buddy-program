// const Paytm = require('../models/paytm');
const uuidv4 = require('uuid/v4');
const credentials = require('../config/paytm-creds');
const meta = require('../config/paytm-meta');
const cs = require('../helpers/checksum');
const axios = require('axios');
const nodemailer = require('nodemailer');
const mailGun = require('nodemailer-mailgun-transport');
var fs = require('fs');
// const Participant = require('../models/participant');
// const Announcement = require('../models/announcements');
// const Notification = require('../models/notifications');
// const Colleges = require('../models/college');
// const Events = require('../models/events');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;


exports.getPayments = (req,res,next) => {
    let oid = req.query.oid;
    let callback = req.query.callback;
    Paytm.findOne({where:{Order_Id:oid}}).then(result => {
        Participant.findOne({where:{id:result.dataValues.Cust_Id}}).then(result => {
            return res.render('events/payments', {
                title:'Payment',
                oid:oid,
                data:result,
                notMain:true,
            })
        })
    })
}

exports.makePayments = (req,res,next) => {
    let err, result, params, url, key;
    if(req.query.retry){
        Participant.findOne({where:{}})
    }
    Paytm.findOne({where:{Order_Id:req.query.orderid}}).then(result => {
        let txn = result;
        params = credentials.staging;
        url = meta.staging.txn_url;
        key = meta.staging.key;
        params["ORDER_ID"] = txn.dataValues.Order_Id;
        params["CUST_ID"] = txn.dataValues.Cust_Id;
        params["TXN_AMOUNT"] = txn.dataValues.Amount;
        params["CALLBACK_URL"] = req.query.callback;
        params["MOBILE_NO"] = txn.dataValues.Contact;
        params["EMAIL"] = txn.dataValues.Email;
        console.log(params);
        cs.genchecksum(params,key,(err, params)=>{
            return res.render('redirect', { url, params, payment: true, });
        });
    })
    .catch(err => {console.log(err)});
    // [err, result] = await to(conn.query("SELECT * FROM paytm WHERE order_id = ? AND status ='OPEN'",[req.query.orderid]));
    // if(err || result.length === 0) {
    //   console.log(err);
    //   return res.render('failure');
    // }
    
}


exports.getPaytmCallback = (req,res,next) => {
    return res.render('events/paytmCallback', {
        title: 'Transaction Status',
        notMain: true,
    })
}

exports.postPaytmCallback = (req,res,next) => {
    return res.redirect('/paytmCallback?oid=' + req.body.ORDERID);
}

exports.getTransactionStatus = (req,res,next) => {
    let err, result, txn, response, url, key;
    Paytm.findOne({where:{Order_Id:req.body.orderid}}).then(result => {
        txn = result.dataValues;
        let query_params = { ORDERID: txn.Order_Id };
        if(txn.status == 'TXN_SUCCESS' || txn.status == 'TXN_FAILURE' || txn.status == 'INVALID') {
            txn.success = true;
            return res.json({ txn });
        }
        // if(txn.type == 'TESTING') {
            query_params.MID = credentials.staging.MID;
            url = meta.staging.status_url;
            key = meta.staging.key;
        /* } else {
            query_params.MID = credentials.production.MID;
            url = meta.production.status_url;
            key = meta.production.key;
        } */
        cs.genchecksum(query_params,key, async (err, query_params) => {
            // [err, response] = await to(axios.post(url,query_params));
            // axios.defaults.headers.post['X-CSRF-Token'] = res.data._csrf;
            // axios.defaults.headers.common['x-csrf-token'] = readCookie('XSRF-TOKEN');
            axios.post(url,query_params).then(response => {
                response = response.data;
                txn.paytmid = response.TXNID;
                txn.banktxnid = response.BANKTXNID;
                txn.amount_paid = response.TXNAMOUNT;
                txn.txndate = response.TXNDATE;
                txn.status = response.STATUS;
                if(response.STATUS == 'TXN_SUCCESS') {
                    if(parseInt(txn.Amount) === parseInt(response.TXNAMOUNT)) {
                        /* if(txn.type == 'SPORTS') {
                            axios.get(
                                'https://sports.mitrevels.in/api/emailstatus/'+req.body.orderid+'?token=arandomafauthtoken'
                            ).then().catch(console.log);
                        } else if (txn.type == 'DASHBOARD' || txn.type == 'TESTING') { */
                            axios.post(
                                'http://localhost:4001/updatePaymentStatus',
                                {
                                order_id: req.body.orderid,
                                txnid: txn.banktxnid,
                                }
                            ).then().catch(console.log);
                        // }
                        //-----------------------------------------------------
                        //Make nodemailer connect here to email reciept
                        //-------------------------------------------------------
                        const auth = {
                            auth: {
                                api_key: process.env.API_KEY || 'key-0bb9d116b294ed0e0a61a379d4a3aeb1', // TODO: 
                                domain: process.env.DOMAIN || 'mailer.manipalmarathon.in' // TODO:
                            }
                        };

                        let transporter = nodemailer.createTransport( mailGun(auth) );
                                transporter.sendMail({
                                        from: 'Events Portal <noreply.events@mitsc.in>', // TODO: email sender
                                        to: `${txn.Email}`, // TODO: email receiver
                                        subject: 'Payment reciept for Events Portal 2021',
                                        html: `
                                                <p>Dear Participant,</p>
                                                <p style='font-weight:bold;'>Please find attached your reciept from Events Portal</p>
                                                <table>
                                                    <tr>
                                                        <th>Customer Id</th>
                                                        <td>${txn.Cust_Id}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Customer Name</th>
                                                        <td>${txn.Cust_Name}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Email</th>
                                                        <td>${txn.Email}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Order Id</th>
                                                        <td>${txn.Order_Id}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Amount Paid</th>
                                                        <td>${txn.amount_paid}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Paytm Id</th>
                                                        <td>${txn.paytmid}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Bank Id</th>
                                                        <td>${txn.banktxnid}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Date of Transaction</th>
                                                        <td>${txn.txndate}</td>
                                                    </tr>
                                                </table>
                                        `
                                    }, (err, data) => {
                                        if (err) {
                                            console.log(err)
                                            return log('Error occurs for', txn.Cust_Id);
                                        }
                                        return log('Email sent for ', txn.Cust_Id);
                                });
                
                            transporter.close();
                        /* await to(axios.post(meta.mailer.url,{
                        email: txn.email,
                        orderid: txn.order_id,
                        amount: txn.amount_paid,
                        paytmid: txn.paytmid,
                        type: txn.type,
                        auth: meta.mailer.key
                        })); */
                    }
                    else txn.status = 'INVALID';
                } else txn.amount_paid = 0.0;
                
                txn.updated_at = undefined;
                delete txn.updated_at;
                Paytm.findOne({where:{Order_Id:txn.Order_Id}})
                    .then(data => {
                        console.log(data);
                        data.created_at = new Date;
                        data.updated_at = txn.updated_at;
                        data.paytmid = txn.paytmid;
                        data.banktxnid = txn.banktxnid;
                        data.status = txn.status;
                        data.amount_paid = txn.amount_paid;
                        data.txndate = txn.txndate;
                        return data.save();
                    }).catch(err => console.log(err))
                // [err, result] = await to(conn.query('UPDATE paytm SET ? WHERE order_id = ?',[txn,txn.order_id]));
                txn.success = true;
                return res.json(txn);
            }).catch(err => console.log(err));
            if(err) return res.json({ success: false });
        });
    }).catch(err => console.log(err));
  /* [err, result] = await to(conn.query('SELECT * FROM paytm WHERE order_id = ?',[req.body.orderid]));
  if(err || !result || result.length == 0) return res.json({ success: false });
  txn = result[0]; */
  
}

exports.updatePaymentStatus = (req,res,next) => {
    let order_id = req.body.order_id , result;
    Participant.findOne({where:{orderid: req.body.order_id}}).then(data => {
        data.paymentStatus = 1;
        data.txnid = req.body.txnid;
        return data.save();
    }).then(result => {
        console.log(result);
        return res.sendSuccess(null, 'Updated details');
    })
    // try {
    //     result = await db.query("UPDATE card_bought SET paid='1' WHERE order_id=?", [order_id]);
    //     if (result.changedRows == 0){
    //     result = await db.query("UPDATE play_card_bought SET paid='1' WHERE order_id=?", [order_id]);
    //     if( result.changedRows > 0 ){
    //         result = await db.query("SELECT * FROM play_card_bought WHERE order_id=?", [order_id]);
    //         if( result.length <= 0 ) return res.sendError(null);
    //         let user = result[0];
    //         result = await db.query("UPDATE informals_users SET no_of_tokens = no_of_tokens + 5 WHERE userid = ?",[ user.delid ]);
    //     }
    //     return res.sendSuccess(null, 'Updated Details');
    //     }
    //     result = await db.query("SELECT * FROM card_bought WHERE order_id = ?", [order_id]);
    //     result = result[0];
    //     // proshow
    //     // console.log(result);
    //     /*if (result.card_type == '8') {
    //     // console.log('in here');
    //     await proshow_add_ticket(result.delid);
    //     }*/
    //     return res.sendSuccess(null, 'Updated details');
    // } catch (err) {
    //     return res.sendError(err);
    // }
}


exports.createAnnouncement = (eventid,clubid,text) => {
    Announcement.create({
        clubid: clubid,
        eventid: eventid,
        announcementtext: text,
    }).then(result => {
        return result;
    })
}

exports.postNotification = (notificationtext, clubid, eventid) => {
    Notification.create({
        clubid: clubid,
        eventid: eventid,
        notificationtext: notificationtext,
        readToken: 0,
    }).then(result => {
        console.log('Notification created')
    }).catch(err => {
        console.log(err);
    })
}

exports.markNotification = (req,res,next) => {
    Notification.destroy({where: {clubid: req.user.clubid}}).then(result => {
        res.json({msg:'All notifications related to the club deleted'});
    });
}

exports.getAnnouncements = (req,res,next) => {
    Announcement.findAndCountAll({order:[['createdAt','DESC']]}).then(result => {
            res.json(result.rows)
        }).catch(err => {console.log(err)})
}


exports.returnColleges = (req,res,next) => {
    Colleges.findAndCountAll().then(colleges => {
        res.json(colleges.rows);
    })
}

exports.getLatestEvents = (req,res,next) => {
    let today = new Date();
    today = today.toISOString().slice(0,10);
    Events.findAll({where:{eventRegToken:1, [Op.or]: {eventdate: {[Op.gte]: today},eventenddate: {[Op.gte]: today}}},limit:3,order:[['id','DESC']]})
    .then(events => {
        res.send(events);
    })
}