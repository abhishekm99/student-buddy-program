const Buds = require('../models/bud');
const Buddy = require('../models/buddy');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const Request = require('../models/request');
const Message = require('../models/messages');
const Branch = require('../models/branches');
const Notification = require('../models/notifications')
const Articles = require('../models/articles');
const { sendEmail } = require('../helpers/email');
const fs = require('fs')

exports.match_interests = async function (a,b,n,e,r,c,branch,userId,res,req){
    let pos=-1;
    var flag = 0;
    let variable = a.split(',');
    let obj;
    Buddy.findAll({where:{budCount:0, gender:b, branch: branch}}).then(buddies => {
        console.log(buddies)
        if(buddies.length === 0){
            console.log('no buddies found')
            return null
        }else{
            for(i=0;i<buddies.length;i++){
                let count = 0;
                let interests = buddies[i].interests.split(',');
                for(j=0;j<variable.length;j++){
                    for(k=0;k<interests.length;k++)
                    {
                        if(interests[k]==variable[j])
                        {
                            count++;
                        }
                    }
                }
                if(count>4)
                {
                    pos = i;
                    flag++;
                    break;
                }
            }
            if(flag==0)
            {
                for(i=0;i<buddies.length;i++){
                    let count = 0;
                    let interests = buddies[i].interests.split(',');
                    for(j=0;j<variable.length;j++){
                        for(k=0;k<interests.length;k++)
                        {
                            if(interests[k]==variable[j])
                            {
                                count++;
                            }
                        }
                    }
                    if(count>0)
                    {
                        pos = i;
                        flag = 1;
                        break;
                    }
                } 
                    
            }
            if(pos>=0)
            {
                Buds.create({
                    name: n,
                    email:e,
                    contactnumber:c,
                    gender:b,
                    branch: branch,
                    reg: r,
                    interests: a,
                    buddyname: buddies[pos].name,
                    buddyemail: buddies[pos].email,
                    buddycontact: buddies[pos].contactnumber,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                    buddyId: buddies[pos].id,
                    userId: userId
                }).then(result => {
                    buddies[pos].budCount = buddies[pos].budCount +1;
                    buddies[pos].save();
                    return result
                }).then(result => {
                    console.log(result);
                    const email = result.dataValues.email;
                    let body = `
                            <center>
                                <h3 style="font-style: italic;color: #919191;">Hello from MITSC!</h3>
                                <h1 style="font-weight: bold;">The system has assigned you a buddy, their details are here in the mail as well as will be displayed on your dashboard</h1>
                                <div class="resulttable" style="text-align: center;">
                                    <table style="width:100%">
                                        <tr>
                                        <th style="border: 1px solid #ddd; padding: 8px;">Name of Buddy</th>
                                        <td style="border: 1px solid #ddd; padding: 8px;">${buddies[pos].name}</td>
                                        </tr>
                                        <tr>
                                        <th style="border: 1px solid #ddd; padding: 8px;">Email</th>
                                        <td style="border: 1px solid #ddd; padding: 8px;">${buddies[pos].email}</td>
                                        </tr>
                                        <tr>
                                        <th style="border: 1px solid #ddd; padding: 8px;">Contact Number</th>
                                        <td style="border: 1px solid #ddd; padding: 8px;">${buddies[pos].contactnumber}</td>
                                        </tr>
                                    </table>
                                </div>
                                <p>If you did not register for the program then please inform us at mitsc20@gmail.com</p>
                            </center>
                    `
                    sendEmail(email, `Student Buddy Program Allotment ${result.dataValues.name}`, body);
                    let bodybuddy = `
                            <center>
                                <h3 style="font-style: italic;color: #919191;">Hello from MITSC!</h3>
                                <h1 style="font-weight: bold;">The system has assigned you a bud, their details are here in the mail as well as will be displayed on your dashboard</h1>
                                <div class="resulttable" style="text-align: center;">
                                <table style="width:100%">
                                    <tr>
                                    <th style="border: 1px solid #ddd; padding: 8px;">Name of Buddy</th>
                                    <td style="border: 1px solid #ddd; padding: 8px;">${result.dataValues.name}</td>
                                    </tr>
                                    <tr>
                                    <th style="border: 1px solid #ddd; padding: 8px;">Email</th>
                                    <td style="border: 1px solid #ddd; padding: 8px;">${result.dataValues.email}</td>
                                    </tr>
                                </table>
                                </div>
                                <p>You can request for their mobile number through the portal to contact them</p>
                                <p>If you did not register for the program then please inform us at mitsc20@gmail.com</p>
                            </center>
                    `
                    sendEmail(result.dataValues.buddyemail, `Student Buddy Program Bud Allotment ${result.dataValues.buddyname}`, bodybuddy);
                    res.json({
                        success: true,
                        msg: 'Thank you, your registration is complete. Please wait to be redirected'
                    })
                    return true
                })
            }
            else{
                Buds.create({
                    name: n,
                    email:e,
                    contactnumber:c,
                    gender:b,
                    branch: branch,
                    reg: r,
                    interests: a,
                    buddyname: buddies[0].name,
                    buddyemail: buddies[0].email,
                    buddycontact: buddies[0].contactnumber,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                    buddyId: buddies[0].id,
                    userId: userId
                }).then(result => {
                    buddies[0].budCount = buddies[0].budCount +1;
                    buddies[0].save();
                    return result
                }).then(result => {
                    console.log(result);
                    const email = result.dataValues.email;
                    let body = `
                            <center>
                                <h3 style="font-style: italic;color: #919191;">Hello from MITSC!</h3>
                                <h1 style="font-weight: bold;">The system has assigned you a buddy, their details are here in the mail as well as will be displayed on your dashboard</h1>
                                <div class="resulttable" style="text-align: center;">
                                <table style="width:100%">
                                    <tr>
                                    <th style="border: 1px solid #ddd; padding: 8px;">Name of Buddy</th>
                                    <td style="border: 1px solid #ddd; padding: 8px;">${buddies[0].name}</td>
                                    </tr>
                                    <tr>
                                    <th style="border: 1px solid #ddd; padding: 8px;">Email</th>
                                    <td style="border: 1px solid #ddd; padding: 8px;">${buddies[0].email}</td>
                                    </tr>
                                    <tr>
                                    <th style="border: 1px solid #ddd; padding: 8px;">Contact Number</th>
                                    <td style="border: 1px solid #ddd; padding: 8px;">${buddies[0].contactnumber}</td>
                                    </tr>
                                </table>
                                </div>
                                <p>If you did not register for the program then please inform us at mitsc20@gmail.com</p>
                            </center>
                    `
                    sendEmail(email, `Student Buddy Program Allotment ${result.dataValues.name}`, body);
                    let bodybuddy = `
                            <center>
                                <h3 style="font-style: italic;color: #919191;">Hello from MITSC!</h3>
                                <h1 style="font-weight: bold;">The system has assigned you a bud, their details are here in the mail as well as will be displayed on your dashboard</h1>
                                <div class="resulttable" style="text-align: center;">
                                <table style="width:100%">
                                    <tr>
                                    <th style="border: 1px solid #ddd; padding: 8px;">Name of Buddy</th>
                                    <td style="border: 1px solid #ddd; padding: 8px;">${result.dataValues.name}</td>
                                    </tr>
                                    <tr>
                                    <th style="border: 1px solid #ddd; padding: 8px;">Email</th>
                                    <td style="border: 1px solid #ddd; padding: 8px;">${result.dataValues.email}</td>
                                    </tr>
                                </table>
                                </div>
                                <p>You can request for their mobile number through the portal to contact them</p>
                                <p>If you did not register for the program then please inform us at mitsc20@gmail.com</p>
                            </center>
                    `
                    sendEmail(result.dataValues.buddyemail, `Student Buddy Program Bud Allotment ${result.dataValues.buddyname}`, bodybuddy);
                    res.json({
                        success: true,
                        msg: 'Thank you, your registration is complete. Please wait to be redirected'
                    })
                    return true
                })
            }
        return true
        }
    }).then(result => {
        console.log('2nd stage reached')
        if(!result){
            Buddy.findAll({where:{budCount:1, gender:b, branch: branch}}).then(buddies => {
                if(buddies.length === 0){
                    res.json({
                        success: false,
                        msg: 'No Buddies Available in your branch, if you would like to get a buddy assigned from another branch please click on the button at the end of the form or else Please contact student council for allotment',
                        nobodyinbranch: true,
                    })
                }else{
                    for(i=0;i<buddies.length;i++){
                        let count = 0;
                        let interests = buddies[i].interests.split(',');
                        for(j=0;j<variable.length;j++){
                            for(k=0;k<interests.length;k++)
                            {
                                if(interests[k]==variable[j])
                                {
                                    count++;
                                }
                            }
                        }
                        if(count>4)
                        {
                            pos = i;
                            flag++;
                            break;
                        }
                    }
                    if(flag==0)
                    {
                        for(i=0;i<buddies.length;i++){
                            let count = 0;
                            let interests = buddies[i].interests.split(',');
                            for(j=0;j<variable.length;j++){
                                for(k=0;k<interests.length;k++)
                                {
                                    if(interests[k]==variable[j])
                                    {
                                        count++;
                                    }
                                }
                            }
                            if(count>0)
                            {
                                pos = i;
                                flag = 1;
                                break;
                            }
                        } 
                            
                    }
                    if(pos>=0)
                    {
                        Buds.create({
                            name: n,
                            email:e,
                            contactnumber:c,
                            gender:b,
                            branch: branch,
                            reg: r,
                            interests: a,
                            buddyname: buddies[pos].name,
                            buddyemail: buddies[pos].email,
                            buddycontact: buddies[pos].contactnumber,
                            createdAt: new Date(),
                            updatedAt: new Date(),
                            buddyId: buddies[pos].id,
                            userId: userId
                        }).then(result => {
                            buddies[pos].budCount = buddies[pos].budCount +1;
                            buddies[pos].save();
                            return result
                        }).then(result => {
                            console.log(result);
                            const email = result.dataValues.email;
                            let body = `
                            <center>
                                <h3 style="font-style: italic;color: #919191;">Hello from MITSC!</h3>
                                <h1 style="font-weight: bold;">The system has assigned you a buddy, their details are here in the mail as well as will be displayed on your dashboard</h1>
                                <div class="resulttable" style="text-align: center;">
                                <table style="width:100%">
                                    <tr>
                                    <th style="border: 1px solid #ddd; padding: 8px;">Name of Buddy</th>
                                    <td style="border: 1px solid #ddd; padding: 8px;">${buddies[pos].name}</td>
                                    </tr>
                                    <tr>
                                    <th style="border: 1px solid #ddd; padding: 8px;">Email</th>
                                    <td style="border: 1px solid #ddd; padding: 8px;">${buddies[pos].email}</td>
                                    </tr>
                                    <tr>
                                    <th style="border: 1px solid #ddd; padding: 8px;">Contact Number</th>
                                    <td style="border: 1px solid #ddd; padding: 8px;">${buddies[pos].contactnumber}</td>
                                    </tr>
                                </table>
                                </div>
                                <p>If you did not register for the program then please inform us at mitsc20@gmail.com</p>
                            </center>
                    `
                    sendEmail(email, `Student Buddy Program Allotment ${result.dataValues.name}`, body);
                    let bodybuddy = `
                            <center>
                                <h3 style="font-style: italic;color: #919191;">Hello from MITSC!</h3>
                                <h1 style="font-weight: bold;">The system has assigned you a bud, their details are here in the mail as well as will be displayed on your dashboard</h1>
                                <div class="resulttable" style="text-align: center;">
                                <table style="width:100%">
                                    <tr>
                                    <th style="border: 1px solid #ddd; padding: 8px;">Name of Buddy</th>
                                    <td style="border: 1px solid #ddd; padding: 8px;">${result.dataValues.name}</td>
                                    </tr>
                                    <tr>
                                    <th style="border: 1px solid #ddd; padding: 8px;">Email</th>
                                    <td style="border: 1px solid #ddd; padding: 8px;">${result.dataValues.email}</td>
                                    </tr>
                                </table>
                                </div>
                                <p>You can request for their mobile number through the portal to contact them</p>
                                <p>If you did not register for the program then please inform us at mitsc20@gmail.com</p>
                            </center>
                    `
                    sendEmail(result.dataValues.buddyemail, `Student Buddy Program Bud Allotment ${result.dataValues.buddyname}`, bodybuddy);
                            res.json({
                                success: true,
                                msg: 'Thank you, your registration is complete. Please wait to be redirected'
                            })
                        })
                    }
                    else{
                        Buds.create({
                            name: n,
                            email:e,
                            contactnumber:c,
                            gender:b,
                            branch: branch,
                            reg: r,
                            interests: a,
                            buddyname: buddies[0].name,
                            buddyemail: buddies[0].email,
                            buddycontact: buddies[0].contactnumber,
                            createdAt: new Date(),
                            updatedAt: new Date(),
                            buddyId: buddies[0].id,
                            userId: userId
                        }).then(result => {
                            buddies[0].budCount = buddies[0].budCount +1;
                            buddies[0].save(); 
                            return result
                        }).then(result => {
                            console.log(result);
                            const email = result.dataValues.email;
                            let body = `
                            <center>
                                <h3 style="font-style: italic;color: #919191;">Hello from MITSC!</h3>
                                <h1 style="font-weight: bold;">The system has assigned you a buddy, their details are here in the mail as well as will be displayed on your dashboard</h1>
                                <div class="resulttable" style="text-align: center;">
                                <table style="width:100%">
                                    <tr>
                                    <th style="border: 1px solid #ddd; padding: 8px;">Name of Buddy</th>
                                    <td style="border: 1px solid #ddd; padding: 8px;">${buddies[0].name}</td>
                                    </tr>
                                    <tr>
                                    <th style="border: 1px solid #ddd; padding: 8px;">Email</th>
                                    <td style="border: 1px solid #ddd; padding: 8px;">${buddies[0].email}</td>
                                    </tr>
                                    <tr>
                                    <th style="border: 1px solid #ddd; padding: 8px;">Contact Number</th>
                                    <td style="border: 1px solid #ddd; padding: 8px;">${buddies[0].contactnumber}</td>
                                    </tr>
                                </table>
                                </div>
                                <p>If you did not register for the program then please inform us at mitsc20@gmail.com</p>
                            </center>
                    `
                    sendEmail(email, `Student Buddy Program Allotment ${result.dataValues.name}`, body);
                    let bodybuddy = `
                            <center>
                                <h3 style="font-style: italic;color: #919191;">Hello from MITSC!</h3>
                                <h1 style="font-weight: bold;">The system has assigned you a bud, their details are here in the mail as well as will be displayed on your dashboard</h1>
                                <div class="resulttable" style="text-align: center;">
                                <table style="width:100%">
                                    <tr>
                                    <th style="border: 1px solid #ddd; padding: 8px;">Name of Buddy</th>
                                    <td style="border: 1px solid #ddd; padding: 8px;">${result.dataValues.name}</td>
                                    </tr>
                                    <tr>
                                    <th style="border: 1px solid #ddd; padding: 8px;">Email</th>
                                    <td style="border: 1px solid #ddd; padding: 8px;">${result.dataValues.email}</td>
                                    </tr>
                                </table>
                                </div>
                                <p>You can request for their mobile number through the portal to contact them</p>
                                <p>If you did not register for the program then please inform us at mitsc20@gmail.com</p>
                            </center>
                    `
                    sendEmail(result.dataValues.buddyemail, `Student Buddy Program Bud Allotment ${result.dataValues.buddyname}`, bodybuddy);
                            res.json({
                                success: true,
                                msg: 'Thank you, your registration is complete. Please wait to be redirected'
                            })
                        })
                    }

                }
            }).then(result => {
                obj = result;
                return obj;
            })
        }
    })
    return obj;
}



exports.match_interests_withoutBranch = async function (a,b,n,e,r,c,branch,userId,res,req){
    let pos=-1;
    var flag = 0;
    let variable = a.split(',');
    let obj;
    Buddy.findAll({where:{budCount:0, gender:b}}).then(buddies => {
        console.log(buddies)
        if(buddies.length === 0){
            console.log('no buddies found')
            return null
        }else{
            for(i=0;i<buddies.length;i++){
                let count = 0;
                let interests = buddies[i].interests.split(',');
                for(j=0;j<variable.length;j++){
                    for(k=0;k<interests.length;k++)
                    {
                        if(interests[k]==variable[j])
                        {
                            count++;
                        }
                    }
                }
                if(count>4)
                {
                    pos = i;
                    flag++;
                    break;
                }
            }
            if(flag==0)
            {
                for(i=0;i<buddies.length;i++){
                    let count = 0;
                    let interests = buddies[i].interests.split(',');
                    for(j=0;j<variable.length;j++){
                        for(k=0;k<interests.length;k++)
                        {
                            if(interests[k]==variable[j])
                            {
                                count++;
                            }
                        }
                    }
                    if(count>0)
                    {
                        pos = i;
                        flag = 1;
                        break;
                    }
                } 
                    
            }
            if(pos>=0)
            {
                Buds.create({
                    name: n,
                    email:e,
                    contactnumber:c,
                    gender:b,
                    branch: branch,
                    reg: r,
                    interests: a,
                    buddyname: buddies[pos].name,
                    buddyemail: buddies[pos].email,
                    buddycontact: buddies[pos].contactnumber,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                    buddyId: buddies[pos].id,
                    userId: userId
                }).then(result => {
                    buddies[pos].budCount = buddies[pos].budCount +1;
                    buddies[pos].save();
                    return result
                }).then(result => {
                    console.log(result);
                    const email = result.dataValues.email;
                    let body = `
                            <center>
                                <h3 style="font-style: italic;color: #919191;">Hello from MITSC!</h3>
                                <h1 style="font-weight: bold;">The system has assigned you a buddy, their details are here in the mail as well as will be displayed on your dashboard</h1>
                                <div class="resulttable" style="text-align: center;">
                                    <table style="width:100%">
                                        <tr>
                                        <th style="border: 1px solid #ddd; padding: 8px;">Name of Buddy</th>
                                        <td style="border: 1px solid #ddd; padding: 8px;">${buddies[pos].name}</td>
                                        </tr>
                                        <tr>
                                        <th style="border: 1px solid #ddd; padding: 8px;">Email</th>
                                        <td style="border: 1px solid #ddd; padding: 8px;">${buddies[pos].email}</td>
                                        </tr>
                                        <tr>
                                        <th style="border: 1px solid #ddd; padding: 8px;">Contact Number</th>
                                        <td style="border: 1px solid #ddd; padding: 8px;">${buddies[pos].contactnumber}</td>
                                        </tr>
                                    </table>
                                </div>
                                <p>If you did not register for the program then please inform us at mitsc20@gmail.com</p>
                            </center>
                    `
                    postNotification('You have been assigned a bud', buddies[pos].id);
                    sendEmail(email, `Student Buddy Program Allotment ${result.dataValues.name}`, body);
                    let bodybuddy = `
                            <center>
                                <h3 style="font-style: italic;color: #919191;">Hello from MITSC!</h3>
                                <h1 style="font-weight: bold;">The system has assigned you a bud, their details are here in the mail as well as will be displayed on your dashboard</h1>
                                <div class="resulttable" style="text-align: center;">
                                <table style="width:100%">
                                    <tr>
                                    <th style="border: 1px solid #ddd; padding: 8px;">Name of Buddy</th>
                                    <td style="border: 1px solid #ddd; padding: 8px;">${result.dataValues.name}</td>
                                    </tr>
                                    <tr>
                                    <th style="border: 1px solid #ddd; padding: 8px;">Email</th>
                                    <td style="border: 1px solid #ddd; padding: 8px;">${result.dataValues.email}</td>
                                    </tr>
                                </table>
                                </div>
                                <p>You can request for their mobile number through the portal to contact them</p>
                                <p>If you did not register for the program then please inform us at mitsc20@gmail.com</p>
                            </center>
                    `
                    sendEmail(result.dataValues.buddyemail, `Student Buddy Program Bud Allotment ${result.dataValues.buddyname}`, bodybuddy);
                    res.json({
                        success: true,
                        msg: 'Thank you, your registration is complete. Please wait to be redirected'
                    })
                    return true
                })
            }
            else{
                Buds.create({
                    name: n,
                    email:e,
                    contactnumber:c,
                    gender:b,
                    branch: branch,
                    reg: r,
                    interests: a,
                    buddyname: buddies[0].name,
                    buddyemail: buddies[0].email,
                    buddycontact: buddies[0].contactnumber,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                    buddyId: buddies[0].id,
                    userId: userId
                }).then(result => {
                    buddies[0].budCount = buddies[0].budCount +1;
                    buddies[0].save();
                    return result
                }).then(result => {
                    console.log(result);
                    const email = result.dataValues.email;
                    let body = `
                            <center>
                                <h3 style="font-style: italic;color: #919191;">Hello from MITSC!</h3>
                                <h1 style="font-weight: bold;">The system has assigned you a buddy, their details are here in the mail as well as will be displayed on your dashboard</h1>
                                <div class="resulttable" style="text-align: center;">
                                <table style="width:100%">
                                    <tr>
                                    <th style="border: 1px solid #ddd; padding: 8px;">Name of Buddy</th>
                                    <td style="border: 1px solid #ddd; padding: 8px;">${buddies[0].name}</td>
                                    </tr>
                                    <tr>
                                    <th style="border: 1px solid #ddd; padding: 8px;">Email</th>
                                    <td style="border: 1px solid #ddd; padding: 8px;">${buddies[0].email}</td>
                                    </tr>
                                    <tr>
                                    <th style="border: 1px solid #ddd; padding: 8px;">Contact Number</th>
                                    <td style="border: 1px solid #ddd; padding: 8px;">${buddies[0].contactnumber}</td>
                                    </tr>
                                </table>
                                </div>
                                <p>If you did not register for the program then please inform us at mitsc20@gmail.com</p>
                            </center>
                    `
                    postNotification('You have been assigned a bud', buddies[0].id);
                    sendEmail(email, `Student Buddy Program Allotment ${result.dataValues.name}`, body);
                    let bodybuddy = `
                            <center>
                                <h3 style="font-style: italic;color: #919191;">Hello from MITSC!</h3>
                                <h1 style="font-weight: bold;">The system has assigned you a bud, their details are here in the mail as well as will be displayed on your dashboard</h1>
                                <div class="resulttable" style="text-align: center;">
                                <table style="width:100%">
                                    <tr>
                                    <th style="border: 1px solid #ddd; padding: 8px;">Name of Buddy</th>
                                    <td style="border: 1px solid #ddd; padding: 8px;">${result.dataValues.name}</td>
                                    </tr>
                                    <tr>
                                    <th style="border: 1px solid #ddd; padding: 8px;">Email</th>
                                    <td style="border: 1px solid #ddd; padding: 8px;">${result.dataValues.email}</td>
                                    </tr>
                                </table>
                                </div>
                                <p>You can request for their mobile number through the portal to contact them</p>
                                <p>If you did not register for the program then please inform us at mitsc20@gmail.com</p>
                            </center>
                    `
                    sendEmail(result.dataValues.buddyemail, `Student Buddy Program Bud Allotment ${result.dataValues.buddyname}`, bodybuddy);
                    res.json({
                        success: true,
                        msg: 'Thank you, your registration is complete. Please wait to be redirected'
                    })
                    return true
                })
            }
        return true
        }
    }).then(result => {
        console.log('2nd stage reached')
        if(!result){
            Buddy.findAll({where:{budCount:1, gender:b}}).then(buddies => {
                if(buddies.length === 0){
                    res.json({
                        success: false,
                        msg: 'No Buddies Available, Please contact student council for allotment',
                    })
                }else{
                    for(i=0;i<buddies.length;i++){
                        let count = 0;
                        let interests = buddies[i].interests.split(',');
                        for(j=0;j<variable.length;j++){
                            for(k=0;k<interests.length;k++)
                            {
                                if(interests[k]==variable[j])
                                {
                                    count++;
                                }
                            }
                        }
                        if(count>4)
                        {
                            pos = i;
                            flag++;
                            break;
                        }
                    }
                    if(flag==0)
                    {
                        for(i=0;i<buddies.length;i++){
                            let count = 0;
                            let interests = buddies[i].interests.split(',');
                            for(j=0;j<variable.length;j++){
                                for(k=0;k<interests.length;k++)
                                {
                                    if(interests[k]==variable[j])
                                    {
                                        count++;
                                    }
                                }
                            }
                            if(count>0)
                            {
                                pos = i;
                                flag = 1;
                                break;
                            }
                        } 
                            
                    }
                    if(pos>=0)
                    {
                        Buds.create({
                            name: n,
                            email:e,
                            contactnumber:c,
                            gender:b,
                            branch: branch,
                            reg: r,
                            interests: a,
                            buddyname: buddies[pos].name,
                            buddyemail: buddies[pos].email,
                            buddycontact: buddies[pos].contactnumber,
                            createdAt: new Date(),
                            updatedAt: new Date(),
                            buddyId: buddies[pos].id,
                            userId: userId
                        }).then(result => {
                            buddies[pos].budCount = buddies[pos].budCount +1;
                            buddies[pos].save();
                            return result
                        }).then(result => {
                            console.log(result);
                            const email = result.dataValues.email;
                            let body = `
                            <center>
                                <h3 style="font-style: italic;color: #919191;">Hello from MITSC!</h3>
                                <h1 style="font-weight: bold;">The system has assigned you a buddy, their details are here in the mail as well as will be displayed on your dashboard</h1>
                                <div class="resulttable" style="text-align: center;">
                                <table style="width:100%">
                                    <tr>
                                    <th style="border: 1px solid #ddd; padding: 8px;">Name of Buddy</th>
                                    <td style="border: 1px solid #ddd; padding: 8px;">${buddies[pos].name}</td>
                                    </tr>
                                    <tr>
                                    <th style="border: 1px solid #ddd; padding: 8px;">Email</th>
                                    <td style="border: 1px solid #ddd; padding: 8px;">${buddies[pos].email}</td>
                                    </tr>
                                    <tr>
                                    <th style="border: 1px solid #ddd; padding: 8px;">Contact Number</th>
                                    <td style="border: 1px solid #ddd; padding: 8px;">${buddies[pos].contactnumber}</td>
                                    </tr>
                                </table>
                                </div>
                                <p>If you did not register for the program then please inform us at mitsc20@gmail.com</p>
                            </center>
                    `
                    postNotification('You have been assigned a bud', buddies[pos].id);
                    sendEmail(email, `Student Buddy Program Allotment ${result.dataValues.name}`, body);
                    let bodybuddy = `
                            <center>
                                <h3 style="font-style: italic;color: #919191;">Hello from MITSC!</h3>
                                <h1 style="font-weight: bold;">The system has assigned you a bud, their details are here in the mail as well as will be displayed on your dashboard</h1>
                                <div class="resulttable" style="text-align: center;">
                                <table style="width:100%">
                                    <tr>
                                    <th style="border: 1px solid #ddd; padding: 8px;">Name of Buddy</th>
                                    <td style="border: 1px solid #ddd; padding: 8px;">${result.dataValues.name}</td>
                                    </tr>
                                    <tr>
                                    <th style="border: 1px solid #ddd; padding: 8px;">Email</th>
                                    <td style="border: 1px solid #ddd; padding: 8px;">${result.dataValues.email}</td>
                                    </tr>
                                </table>
                                </div>
                                <p>You can request for their mobile number through the portal to contact them</p>
                                <p>If you did not register for the program then please inform us at mitsc20@gmail.com</p>
                            </center>
                    `
                    sendEmail(result.dataValues.buddyemail, `Student Buddy Program Bud Allotment ${result.dataValues.buddyname}`, bodybuddy);
                            res.json({
                                success: true,
                                msg: 'Thank you, your registration is complete. Please wait to be redirected'
                            })
                        })
                    }
                    else{
                        Buds.create({
                            name: n,
                            email:e,
                            contactnumber:c,
                            gender:b,
                            branch: branch,
                            reg: r,
                            interests: a,
                            buddyname: buddies[0].name,
                            buddyemail: buddies[0].email,
                            buddycontact: buddies[0].contactnumber,
                            createdAt: new Date(),
                            updatedAt: new Date(),
                            buddyId: buddies[0].id,
                            userId: userId
                        }).then(result => {
                            buddies[0].budCount = buddies[0].budCount +1;
                            buddies[0].save(); 
                            return result
                        }).then(result => {
                            console.log(result);
                            const email = result.dataValues.email;
                            let body = `
                            <center>
                                <h3 style="font-style: italic;color: #919191;">Hello from MITSC!</h3>
                                <h1 style="font-weight: bold;">The system has assigned you a buddy, their details are here in the mail as well as will be displayed on your dashboard</h1>
                                <div class="resulttable" style="text-align: center;">
                                <table style="width:100%">
                                    <tr>
                                    <th style="border: 1px solid #ddd; padding: 8px;">Name of Buddy</th>
                                    <td style="border: 1px solid #ddd; padding: 8px;">${buddies[0].name}</td>
                                    </tr>
                                    <tr>
                                    <th style="border: 1px solid #ddd; padding: 8px;">Email</th>
                                    <td style="border: 1px solid #ddd; padding: 8px;">${buddies[0].email}</td>
                                    </tr>
                                    <tr>
                                    <th style="border: 1px solid #ddd; padding: 8px;">Contact Number</th>
                                    <td style="border: 1px solid #ddd; padding: 8px;">${buddies[0].contactnumber}</td>
                                    </tr>
                                </table>
                                </div>
                                <p>If you did not register for the program then please inform us at mitsc20@gmail.com</p>
                            </center>
                    `                    
                    postNotification('You have been assigned a bud', buddies[0].id);
                    sendEmail(email, `Student Buddy Program Allotment ${result.dataValues.name}`, body);
                    let bodybuddy = `
                            <center>
                                <h3 style="font-style: italic;color: #919191;">Hello from MITSC!</h3>
                                <h1 style="font-weight: bold;">The system has assigned you a bud, their details are here in the mail as well as will be displayed on your dashboard</h1>
                                <div class="resulttable" style="text-align: center;">
                                <table style="width:100%">
                                    <tr>
                                    <th style="border: 1px solid #ddd; padding: 8px;">Name of Buddy</th>
                                    <td style="border: 1px solid #ddd; padding: 8px;">${result.dataValues.name}</td>
                                    </tr>
                                    <tr>
                                    <th style="border: 1px solid #ddd; padding: 8px;">Email</th>
                                    <td style="border: 1px solid #ddd; padding: 8px;">${result.dataValues.email}</td>
                                    </tr>
                                </table>
                                </div>
                                <p>You can request for their mobile number through the portal to contact them</p>
                                <p>If you did not register for the program then please inform us at mitsc20@gmail.com</p>
                            </center>
                    `
                    sendEmail(result.dataValues.buddyemail, `Student Buddy Program Bud Allotment ${result.dataValues.buddyname}`, bodybuddy);
                            res.json({
                                success: true,
                                msg: 'Thank you, your registration is complete. Please wait to be redirected'
                            })
                        })
                    }

                }
            }).then(result => {
                obj = result;
                return obj;
            })
        }
    })
    return obj;
}


exports.raiseRequest = (req,res,next) => {
    var buddyid = req.body.buddyid;
    var budid = req.body.budid;
    Request.create({
        buddyId: buddyid,
        budId: budid,
        permission: 0,
        createdAt: new Date(),
        updatedAt: new Date()
    }).then(result => {
        Buds.findByPk(budid).then(bud => {
            let body = `
                    <center>
                        <h3 style="font-style: italic;color: #919191;">Hello from Student Buddy Program</h3>
                        <h1 style="font-weight: bold;">There is a new message from your buddy, please visit the portal to reply</h1>
                        <p><bold>Message: </bold>Your buddy has requested to view your contact number, please approve the request on the portal in the <bold>Contact Buddy through Portal</bold> section</p>
                    </center>
            `
            postNotification('Your buddy has requested to view your contact number, please go to contact buddy through portal section to approve', bud.userId);
            sendEmail(bud.email, `Student Buddy Message from ${req.user.name}`, body);
            Message.create({
                senderName: req.user.name,
                senderUserId: req.user.id,
                buddyId: result.buddyId,
                budId: result.budId,
                message: `Your buddy ${req.user.name} has requested to view your contact number, please click
                on the <a href="https://studentbuddy.mitportals.in/admin/approverequest?budid=${result.budId}&buddyid=${result.buddyId}">link</a> to approve the request`,
                createdAt: new Date(),
                updatedAt: new Date()
            }).then(result => {
                res.json(result);
            })
        })
    })
}

exports.getRequestData = (req,res,next) => {
    Request.findOne({where:{buddyId: req.query.buddyid, budId: req.query.budid}}).then(request => {
        if(!request){
            res.json({
                permission: -1,
                msg: 'Request yet to be raised'
            })
        }else{
            if(request.permission === true){
                res.json({
                    permission: 1,
                    msg: 'Permitted to recieve details'
                })
            }else{
                res.json({
                    permission: 0,
                    msg: 'Not Permitted yet'
                })
            }
        }
    })
}

exports.approveContactRequest = (req,res,next) => {
    let budid = req.query.budid;
    let buddyid = req.query.buddyid;
    Buds.findOne({where: {id: budid}}).then(bud => {
        if(bud.userId === req.user.id){
            Request.findOne({where: {budId: budid, buddyId: buddyid}}).then(request => {
                request.permission = 1;
                request.updatedAt = new Date();
                request.save();
            }).then(result => {
                let body = `
                <center>
                    <h3 style="font-style: italic;color: #919191;">Hello from Student Buddy Program</h3>
                    <h1 style="font-weight: bold;">There is a new message from your buddy, please visit the portal to reply</h1>
                    <p><bold>Message: </bold>Your bud has approved the request to view their contact number, please visit the portal for the contact number</p>
                </center>
                `
                Buddy.findByPk(buddyid).then(buddy =>{
                    postNotification('Your bud has approved the request for you to view their contact', buddy.userId);
                })
                sendEmail(bud.buddyemail, `Student Buddy Message from ${req.user.name}`, body);
                req.flash('success', 'Approved contact request');
                req.session.save(function () {
                    res.redirect('/admin/dashboard')
                });
            })
        }else{
            res.redirect('/admin/dashboard');
        }
    })
    
}

exports.createMessage = (req,res,next) => {
    let budid = req.body.budid;
    let buddyid = req.body.buddyid;
    let message = req.body.message;

    if(req.user.utype === 'bud'){
        Buddy.findByPk(buddyid).then(buddy => {
            let body = `
                    <center>
                        <h3 style="font-style: italic;color: #919191;">Hello from Student Buddy Program</h3>
                        <h1 style="font-weight: bold;">There is a new message from your buddy, please visit the portal to reply</h1>
                        <p><bold>Message: </bold>${message}</p>
                    </center>
            `
            postNotification('You have a new message from your bud', buddy.userId);
            sendEmail(buddy.email, `Student Buddy Message from ${req.user.name}`, body);
            Message.create({
                senderName: req.user.name,
                senderUserId: req.user.id,
                budId: budid,
                buddyId: buddyid,
                message: message,
                createdAt: new Date(),
                updatedAt: new Date(),
            }).then(result => {
                res.json({
                    success: true,
                    msg: 'Message sent'
                })
            }).catch(error => {
                res.json({
                    success: false,
                    msg: error
                })
            })
        })
    }else{
        Buds.findByPk(budid).then(bud => {
            let body = `
                    <center>
                        <h3 style="font-style: italic;color: #919191;">Hello from Student Buddy Program</h3>
                        <h1 style="font-weight: bold;">There is a new message from your bud, please visit the portal to reply</h1>
                        <p><bold>Message: </bold>${message}</p>
                    </center>
            `
            postNotification('You have a new message from your buddy', bud.userId);
            sendEmail(bud.email, `Student Buddy Message from ${req.user.name}`, body);
            Message.create({
                senderName: req.user.name,
                senderUserId: req.user.id,
                budId: budid,
                buddyId: buddyid,
                message: message,
                createdAt: new Date(),
                updatedAt: new Date(),
            }).then(result => {
                res.json({
                    success: true,
                    msg: 'Message sent'
                })
            }).catch(error => {
                res.json({
                    success: false,
                    msg: error
                })
            })
        })
    }
}

exports.getMessages = (req,res,next) => {
    let budid = req.query.budid;
    let buddyid = req.query.buddyid;
    if(req.user.utype === 'bud'){
        Buds.findOne({where:{userId: req.user.id}}).then(bud => {
            if(bud.userId === req.user.id){
                Message.findAll({where: {buddyId: bud.buddyId, budId: bud.id}, order:[['createdAt','DESC']]}).then(messages => {
                    if(messages){
                        res.json({
                            success: true,
                            message: messages
                        })
                    }
                    else{
                        res.json({
                            success: false,
                            message: 'No messages'
                        })
                    }
                })
            }else{
                res.json({
                    success: false,
                    message: 'Failed to fetch messages'
                })
            }
        })
    }else if(req.user.utype === 'buddy'){
        Buddy.findByPk(buddyid).then(buddy => {
            if(buddy.userId === req.user.id){
                Message.findAll({where: {buddyid: buddyid, budid: budid}, order:[['createdAt','DESC']]}).then(messages => {
                    if(messages){
                        res.json({
                            success: true,
                            message: messages
                        })
                    }
                    else{
                        res.json({
                            success: false,
                            message: 'No messages'
                        })
                    }
                })
            }else{
                res.json({
                    success: false,
                    message: 'Failed to fetch messages'
                })
            }
        })
    }else{
        res.json({
            success: false,
            message: 'Failed to fetch messages'
        })
    }
    
}

exports.getBranchList = (req,res,next) => {
    Branch.findAll().then(branches => {
        res.json(branches);
    })
}

function postNotification (notificationtext, id) {
    Notification.create({
        userid: id,
        notificationtext: notificationtext,
        readToken: 0,
    }).then(result => {
        console.log('Notification created')
    }).catch(err => {
        console.log(err);
    })
}

exports.markNotification = (req,res,next) => {
    Notification.destroy({where: {userid: req.user.id}}).then(result => {
        res.json({msg:'All notifications related to the club deleted'});
    });
}

exports.getCounts = (req,res,next) => {
    var obj = {};
    Buddy.count({where:{gender:'Male'}}).then(result => {
        obj.buddymale = result;
        Buddy.count({where:{gender:'Female'}}).then(result => {
            obj.buddyfemale = result;
            Buds.count({where:{gender:'Male'}}).then(result => {
                console.log(result)
                obj.budmale = result;
                Buds.count({where:{gender:'Female'}}).then(result => {
                    obj.budfemale = result;
                    return obj
                }).then(result => {
                    res.json(obj)
                })
            })
        })
    })
}

exports.getArticles= (req,res,next) => {
    Articles.findAll().then(articles => {
        res.json(articles);
    })
}